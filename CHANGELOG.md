# Changelog

## v0.1.12 - 2021-10-14

- Fix config for PHPStan
- Update tools versions and config
- Clean code refactor
- Refactor Gitlab CI config
- Fix composer PSR-4 config

## v0.1.1 - 2021-10-07

- Refactor for a better code
- Place code into a common source path
- **Fixtures** now are under **Lump** packet
- Update dependencies

## v0.1.0 - 2020-11-17

- Initial release
  * **Core** implementation with main layers defined for clean architecture
  * **Lump** library for support with main services for *core* package
  * **Fixtures** tool for create domain entities fakers
