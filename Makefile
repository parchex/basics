### Components config selection
##
# Docker (choose one... )
#  values: docker-compose | docker-engine (default)
MAKEUP_HAS += docker-engine
##
# Merge release (choose one... )
#  values:  git-flow | git-trunk (default)
MAKEUP_HAS += git-trunk
##
# PHP options...
#  values: php-composer php-build
#          php-qa
#          php-doctrine php-symfony
#          php-tests-xunit php-tests-mutant php-tests-rspec php-tests-bdd
MAKEUP_HAS += php-composer php-tests-rspec php-qa

### Environment configuration...
##
# PHP_CLI = ${PHP_CLI_LOCAL} # For PHP local installation
##
# params: QA_REPORTS | QA_REPORTS_DISABLED
# values: phpstan-report php-cs-fixer-report phpcs-report phpmd-report phpcpd-report phploc-report phpmetrics
QA_REPORTS_DISABLED = phpmd-report
#
PHPSTAN_OPTIONS  := --memory-limit=256M

### Tag rules to update files with new version
TAG_APP_FILES += dot.env
dot.env.tag: TAG_REG_EXP=/APP_VERSION=.*/APP_VERSION=${TAG}/

TAG_APP_FILES += cli/fixture-builder
cli/fixture-builder.tag: TAG_REG_EXP=/app_version[ ]*=.*/app_version = '${TAG}';/

###
include make/MakeUp.mk
