<?php

$config = new \PhpCsFixer\Config();
return $config
    ->setCacheFile(__DIR__ . '/var/cache/php_cs.cache')
    ->setRules(
        [
            '@PSR2' => true,
            'array_syntax' => ['syntax' => 'short'],
            'phpdoc_align' => false,
        ]
    )
    ->setFinder(
        \PhpCsFixer\Finder::create()
            ->in(__DIR__ . '/src/dev')
    );
