# Php Architecture Hexagonal - Componentes para "Hexagonal Architecture" y "DDD"

Componentes básicos para el desarrollo de aplicaciones diseñadas con "DDD" y "Hexagonal Architecture"

## Requisitos

Instalar Docker para poder ejecutar la versión concreta para el proyecto de PHP...

https://docs.docker.com/engine/installation/

## Instalación 

Instalacíón e inicialización del proyecto

```bash
make install
```

Con esto tendríamos desplegado el proyecto con las dependencias y herramientas
necesarias para su ejecución...

* Utilidades necesarias para desarrollo y pruebas en el directorio `bin`...
  * composer
  * testing tools (*kahlan*)
* Instalación de dependencias (con *composer*)

**[!]** Para generar la configuración para un entorno de desarrollo mejor con `make install-dev`

**[!]** Sí quisiéramos borrar la instalación generada podemos recurrir ejecutando `make uninstall`

### Instalación con Docker

```bash
make install-docker-dev
```

Instala el proyecto generando la imagen necesaria de PHP con Docker donde se ejecutaran
todos los procesos PHP, en concreto todos los tests desarrollados que prueban el proyecto.

## Ejecución

Están disponibles diferentes _targets_ en **make** que simplifican las tareas
comunes y uso de las principales herramientas de desarrollo y pruebas...

```bash
make help
```

A través del fichero de **`aliases.sh`** podemos disponer de varios comandos de para ejecutar PHP
usando la virtualización con Docker para usar la versión concreta del proyecto...

```bash
source aliases.sh
```

* **php-cli**: Podemos ejecutar cualquier script PHP con la versión PHP del proyecto.
```bash
php-cli bin/kahlan
php-cli bin/composer
```
* **composer**: lanza composer usando la version de PHP del contenedor de la aplicaión
```bash
composer update
composer show
```

## Testing

Para poder ejecutar el set de pruebas generadas tenemos...

```bash
make spec
make spec-verbose
make spec-coverage
```

O a través del alias en el fichero `alieases.sh`...

```bash
kahlan
```

## Construido con 🛠️

* [Docker](https://www.docker.com/) - Virtualización
* [Composer](https://getcomposer.org/) - Gestor de dependencias
* [Kahlan](https://kahlan.github.io/docs/) - BDD & Spec test framework 

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. 
Para todas las versiones disponibles, 
mira los [tags en el repositorio](https://gitlab.com/parchex/basics/tags).

## Licencia 📄

Este proyecto está bajo la Licencia (WTFPL) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

---
⌨️ con ❤️ por [Oxkhar](https://oxkhar.com/) 😊
