#!/bin/bash

. make/aliases-install.sh

[[ -e .env && -e .env.docker ]] || aliases-env dot.env dot.env.docker
[ -e .env ] && . .env
[ -e .env.docker ] && . .env.docker

## Declare here env variables to configure
: ${DOCKER_IMAGE_RUN:=${DOCKER_REGISTRY}${DOCKER_IMAGE_NAME}:${APP_VERSION}}
# : ${DOCKER_SERVICE_RUN:=cli}
## Choose one...
# : ${PHP_CLI:="php"}
# : ${PHP_CLI:="php-docker"} # Default
# : ${PHP_CLI:="php-docker-compose"}
## For debugging...
# : ${PHP_DEBUG:="php-xdebug"} # Default
# : ${PHP_DEBUG:="php-xdebug3"}

##
. make/aliases-docker.sh
. make/aliases-php.sh
# . make/aliases-server.sh
