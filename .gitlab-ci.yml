stages:
  - image
  - install
  - qa
  - test
  - public
  - release

docker image:
  stage: image
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE
  except:
    - tags
  only:
    changes:
      - Dockerfile
  cache: [ ]

default:
  image: $CI_REGISTRY_IMAGE
  cache:
    - key: vendor
      paths:
        - bin/
        - vendor/

variables:
  GIT_SUBMODULE_STRATEGY: normal
  PHP_CLI: php
  COMPOSER_HOME: var/composer
  COMPOSER_OPTIONS: --no-progress --prefer-dist --ignore-platform-reqs --optimize-autoloader
  REPORT: build/report

composer:
  stage: install
  cache:
    - key: composer
      paths:
        - var/composer/
    - key: vendor
      paths:
        - bin/
        - vendor/
  script:
    - make install install-qa-tools

phpstan:
  stage: qa
  allow_failure: false
  script:
    - make phpstan-report
  artifacts:
    when: always
    reports:
      junit: ${REPORT}/phpstan/junit.xml
      codequality: ${REPORT}/phpstan/gitlab.json
    paths:
      - ${REPORT}/phpstan/
    expire_in: 1 week

mess-detector:
  stage: qa
  allow_failure: true
  variables:
    PHPMD_RULES: unusedcode,cleancode,controversial,design
  script:
    - make phpmd-report
  artifacts:
    paths:
      - ${REPORT}/phpmd/
    expire_in: 1 week

copy-paste-detector:
  stage: qa
  allow_failure: true
  script:
    - make phpcpd-report
  artifacts:
    paths:
      - ${REPORT}/phpcpd/
    expire_in: 1 week

phploc:
  stage: qa
  allow_failure: true
  script:
    - make phploc-report
  artifacts:
    paths:
      - ${REPORT}/phploc/
    expire_in: 1 week

php-cs-fixer:
  stage: qa
  allow_failure: true
  script:
    - make php-cs-fixer-report
  artifacts:
    reports:
      junit: ${REPORT}/php-cs-fixer/junit.xml
    paths:
      - ${REPORT}/php-cs-fixer/
    expire_in: 1 week

php-codesniffer:
  stage: qa
  allow_failure: true
  script:
    - make phpcs-report
  artifacts:
    reports:
      junit: ${REPORT}/codesniffer/junit.xml
    paths:
      - ${REPORT}/codesniffer/
    expire_in: 1 week

php-metrics:
  stage: qa
  script: make phpmetrics
  artifacts:
    paths:
      - ${REPORT}/metrics/
  allow_failure: true

test:
  stage: test
  dependencies: [ ]
  variables:
    GENHTML: genhtml
  before_script:
    - make install
  script:
    - make clean spec-report
  except:
    - tags
  artifacts:
    paths:
      - ${REPORT}

pages:
  stage: public
  script:
    - mkdir -p public
    - cp -r ${REPORT}/* public/
  artifacts:
    paths:
      - ${REPORT}
      - public
  cache: [ ]

prepare_release:
  stage: public
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - make bin/chag
    - echo "# Changelog" > changelog.txt
    - bin/chag contents >> changelog.txt
  artifacts:
    paths:
      - changelog.txt

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_release
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - echo "Release $CI_COMMIT_TAG"
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" \
                         --description "$(cat changelog.txt)" \
                         --tag-name "$CI_COMMIT_TAG"  \
                         --ref "$CI_COMMIT_TAG"
  cache: [ ]
