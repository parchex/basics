FROM php:7.3-cli
MAINTAINER Oxkhar "dev@oxkhar.com"

USER root
RUN DEBIAN_FRONTEND=noninteractive \
    && apt-get update  \
    && apt-get install -y --no-install-recommends unzip git lcov libicu-dev libzip-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install -j$(nproc) bcmath zip intl \
    && pecl install xdebug-2.9.8 \
    && mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# For debugging configuration...
# - For linux have to change remote host to ip (host.docker.internal only form mac)
# - Execute php with "-d zend_extension=xdebug.so" to run with debug mode
COPY config/docker/xdebug.ini ${PHP_INI_DIR}/conf.d/xdebug.ini
COPY config/docker/php.ini ${PHP_INI_DIR}/conf.d/20-app.ini

WORKDIR /app

ARG COMPOSER_HOME
ENV COMPOSER_HOME ${COMPOSER_HOME:-/home/composer}

RUN mkdir -p ${COMPOSER_HOME} && chmod 777 ${COMPOSER_HOME}

ARG USER
ENV USER ${USER:-root}
USER $USER

# The built-in PHP webserver only responds to SIGINT, not to SIGTERM
# STOPSIGNAL SIGINT
