<?php

namespace Spec\Parchex\Lump\Events;

use Parchex\Common\DateTime;
use Parchex\Core\Domain\DomainEvent;
use Parchex\Lump\Events\Persistence\AllEventsListener;
use Parchex\Lump\Events\Persistence\EventStorage;
use Parchex\Lump\Events\Persistence\Event;
use Parchex\Lump\Events\Persistence\PersistAllEventsEmittedMiddleware;
use Parchex\Lump\Events\Subscriber;
use Kahlan\Arg;
use Kahlan\Plugin\Double;

describe("Events", function () {
    describe("Event data structure", function () {
        it("records date and time when event occurred", function () {
            $before = DateTime::now();

            $event = new EventTested();

            $after = DateTime::now();

            expect($event->occurredOn())
                ->toBeGreaterThan($before)
                ->toBeLessThan($after);
        });

        it("names event like FQN class", function () {
            $event = new EventTested();

            expect($event->name())->toBe(EventTested::class);
        });

        it("serializes data event in json format", function () {
            $event = new EventTested();

            expect(json_encode($event))->toBeA("string");
        });
    });

    describe("Event storage process", function () {
        given("subscriber", function () {
            $subscriber = Double::instance(
                ["implements" => Subscriber::class]
            );
            allow($subscriber)
                ->toReceive("attach")
                ->andReturn($subscriber);

            return $subscriber;
        });
        given("listener", function () {
            $listener = new AllEventsListener();
            allow($listener)
                ->toReceive("release")
                ->andReturn([
                    Double::instance(["extends" => DomainEvent::class]),
                    Double::instance(["extends" => DomainEvent::class])
                ]);

            return $listener;
        });
        given("storage", function () {
            return Double::instance(
                ["implements" => EventStorage::class]
            );
        });
        given("middleware", function () {
            return new PersistAllEventsEmittedMiddleware(
                $this->storage,
                $this->subscriber,
                $this->listener
            );
        });
        it("subscribes the listener for all events occurred in main action", function () {
            expect($this->subscriber)
                ->toReceive("attach")
                ->with(Arg::toBe("*"), $this->listener)->ordered;

            expect($this->subscriber)
                ->toReceive("detach")
                ->with(Arg::toBe("*"), $this->listener)->ordered;

            $this->middleware->handle("Message", "trim");
        });
        it("recovers all events from listener and append to event storage", function () {
            expect($this->listener)->toReceive("release")->ordered;

            expect($this->storage)
                ->toReceive("append")
                ->with(
                    Arg::toBeAnInstanceOf(Event::class)
                )
                ->times(2);

            $this->middleware->handle("Message", "trim");
        });
    });
});

class EventTested extends DomainEvent
{
}
