<?php

namespace Spec\Parchex\Lump\Events;

use Kahlan\Plugin\Double;
use League\Event\EmitterInterface;
use League\Event\ListenerInterface;
use Parchex\Lump\Events\Event;
use Parchex\Lump\Events\League\Manager;

describe("Event Manager for League Event Emitter", function () {
    it("is a publisher for emit events", function () {
        /** @var EmitterInterface $emitter */
        $emitter = Double::instance(
            ['implements' => EmitterInterface::class]
        );
        $event = Double::instance(
            ["implements" => Event::class]
        );
        $manager = new Manager($emitter);

        expect($emitter)->toReceive("emit")->with($event);

        $manager->trigger($event);
    });

    it("is a subscriber for attach/detach listeners to a event", function () {
        /** @var EmitterInterface $emitter */
        $emitter = Double::instance(
            ['implements' => EmitterInterface::class]
        );
        $listener = Double::instance(
            ['implements' => ListenerInterface::class]
        );
        $manager = new Manager($emitter);

        expect($emitter)
            ->toReceive("addListener")
            ->with(Event::class, $listener);

        $manager->attach(Event::class, $listener);

        expect($emitter)
            ->toReceive("removeListener")
            ->with(Event::class, $listener);

        $manager->detach(Event::class, $listener);

        expect($emitter)
            ->toReceive("removeAllListeners");

        $manager->loose(Event::class);
    });
});

