<?php

namespace Spec\Parchex\Lump\Fixtures\Charger;

use Faker\Generator;
use Parchex\Lump\Fixtures\Charger\Filler;
use Parchex\Lump\Fixtures\Charger\Value;

describe("Filler of faker values", function () {
    given("generator", function () {
        $generator = new Generator();
        allow($generator)
            ->toReceive("format")
            ->with("name")
            ->andReturn("Pepe");
        allow($generator)
            ->toReceive("format")
            ->with("email")
            ->andReturn("hi@pepe.com");

        return $generator;
    });
    given("filler", function () {
        return new Filler(["name", "email"], $this->generator);
    });
    describe("generator of values", function () {
        it("generates fake values with format of fields specified", function () {
            expect($this->generator)
                ->toReceive("format")
                ->with("name");
            expect($this->generator)
                ->toReceive("format")
                ->with("email");

            $this->filler;
        });
        it("refills new values generating with format fields", function () {
            expect($this->generator)
                ->toReceive("format")
                ->with("name")->times(2);
            expect($this->generator)
                ->toReceive("format")
                ->with("email")->times(2);

            $this->filler->refill();
        });
        it("generates array mapping with fields and values generated", function () {
            expect($this->filler->toArray())
                ->toBe([
                    "name" => "Pepe",
                    "email" => "hi@pepe.com"
                ]);
        });
    });
    describe("filling fields with a specific value", function () {
        it("fills with value for a specific field", function () {
            $this->filler->withValue("name", "Manolo");

            expect($this->filler->toArray())
                ->toBe([
                    "name" => "Manolo",
                    "email" => "hi@pepe.com"
                ]);
        });
        it("fills with value only for fields specified", function () {
            // age field not exist in filler
            $this->filler->withValue("age", 34);

            expect($this->filler->toArray())
                ->toBe([
                    "name" => "Pepe",
                    "email" => "hi@pepe.com"
                ]);
        });
        it("fills with a representation value for a specific field", function () {
            $this->filler->with("name", Value::set("Manolo"));

            expect($this->filler->toArray())
                ->toBe([
                    "name" => "Manolo",
                    "email" => "hi@pepe.com"
                ]);
        });
    });
    describe("access and alter values", function () {
        it("gives values of a field", function () {
            expect($this->filler->valueOf("name"))
                ->toEqual(Value::set("Pepe"));
            expect($this->filler->valueOf("email"))
                ->toEqual(Value::set("hi@pepe.com"));
        });
        it("gives null value for field that not exists", function () {
            expect($this->filler->valueOf("notExists"))
                ->toEqual(Value::set(null));
        });
        it("changes the type value through value modifiers", function () {
            $this->filler
                ->withValue("name", "null")
                ->valueOf("name")
                ->nullable();
            $this->filler
                ->withValue("email", "hi@mail.com,hola@correo.es")
                ->valueOf("email")
                ->collection();

            expect($this->filler->toArray())
                ->toEqual([
                    "name" => null,
                    "email" => ["hi@mail.com","hola@correo.es"]
                ]);
        });
    });
});
