<?php

namespace Spec\Parchex\Lump\Fixtures\Charger;

use Parchex\Lump\Fixtures\Charger\Value;

describe("Value representation", function () {
    describe("Assign values representations", function () {
        it("sets a value with a new value representation", function () {
            expect(Value::set("new value"))
                ->toBeAnInstanceOf(Value::class);
        });

        it("gets a value represented", function () {
            $val = Value::set(1234567890);

            expect($val->get())->toBe(1234567890);
        });

        it("represents any type of value", function () {
            expect(Value::set("string value")->get())->toBe("string value");
            expect(Value::set("10")->get())->toBe("10");
            expect(Value::set("null")->get())->toBe("null");
            expect(Value::set("1,2,3,4")->get())->toBe("1,2,3,4");
            expect(Value::set(4)->get())->toBe(4);
            expect(Value::set(2.3)->get())->toBe(2.3);
            expect(Value::set(true)->get())->toBe(true);
            $obj = new \stdClass();
            expect(Value::set($obj)->get())->toBe($obj);
        });
    });

    describe("Transform values", function () {
        describe("boolean values", function () {
            given("booleanValues", function () {
                return [
                    "1|0" => ["1", "0"],
                    "on|off" => ["yes","no"],
                    "true|false" => ["true", "false"],
                    "yes|no" => ["yes", "no"],
                    "primitive" => [true, false]
                ];
            });
            with_provided_data_it("converts to boolean", $this->booleanValues, function ($true, $false) {
                expect(Value::set($true)->boolean()->get())->toBe(true);
                expect(Value::set($false)->boolean()->get())->toBe(false);
            });

            it("converts to null value with any not represented boolean value", function () {
                expect(Value::set("y")->boolean()->get())->toBeNull();
            });
        });

        describe("nullable values", function () {
            it("converts to null value", function () {
                expect(Value::set("null")->nullable()->get())->toBeNull();
                expect(Value::set(null)->nullable()->get())->toBeNull();
            });
            it("lets same value if not is a null representation", function () {
                expect(Value::set("nil")->nullable()->get())->toBe("nil");
            });
        });

        describe("object instances values", function () {
            it("instances a new object form a class and factory method", function () {
                $value = Value::set("name")
                    ->instance(
                        \stdClass::class,
                        function ($value, $class) {
                            $obj = new $class();
                            $obj->prop = $value;
                            return $obj;
                        }
                    );

                expect($value->get())->toBeAnInstanceOf(\stdClass::class);
                expect($value->get()->prop)->toBe("name");
            });
            it("instances only values that are not null", function () {
                $value = Value::set("null")
                    ->instance(
                        \stdClass::class,
                        function () {
                            // not executed
                        }
                    );

                expect($value->get())->toBeNull();
            });
            it("instances a collection of values", function () {
                $value = Value::set(["one", "two", "three"])
                    ->instance(
                        \stdClass::class,
                        function ($value, $class) {
                            $obj = new $class();
                            $obj->prop = $value;
                            return $obj;
                        }
                    );

                $expected = [
                    (object)["prop" => "one"],
                    (object)["prop" => "two"],
                    (object)["prop" => "three"]
                ];
                expect($value->get())->toEqual($expected);
            });
        });

        describe("identifiers values", function () {
            it("instance a new ID from a class identifier and value", function () {
                $value = Value::set("111-222-333-44")
                    ->identifier(Id::class);

                expect($value->get())
                    ->toBeAnInstanceOf(Id::class)
                    ->toEqual(Id::fromString("111-222-333-44"));
            });
        });

        describe("collection of values", function () {
            given("listItems", function () {
                return [
                   "; token" => ["1;2;3;4;5"],
                   ", token" => ["1,2,3,4,5"],
                   "| token" => ["1|2|3|4|5"],
                   "space token" => ["1 2 3 4 5"],
                ];
            });
            with_provided_data_it("converts list values in a string in a collection", $this->listItems, function ($list) {
                $value = Value::set($list)->collection();

                expect($value->get())
                    ->toBeAn("array")
                    ->toBe(["1", "2", "3", "4", "5"]);
            });
            it("converts a object value in a one element collection", function () {
                $obj = (object)["prop" => "name"];
                $value = Value::set($obj)->collection();

                expect($value->get())
                    ->toBeAn("array")
                    ->toHaveLength(1)
                    ->toContain($obj);
            });
            it("maintains a already collection value", function () {
                $list = ["one", "two","three"];
                $value = Value::set($list)->collection();

                expect($value->get())
                    ->toBe($list);
            });
        });
    });
    describe("Checking values", function () {
        it("is null", function () {
            expect(Value::set(null)->isNull())->toBe(true);
            expect(Value::set("null")->isNull())->toBe(true);
            expect(Value::set("something")->isNull())->toBe(false);
        });
        it("is instance of class", function () {
            expect(Value::set(new \stdClass())->isInstanceOf(\stdClass::class))
                ->toBe(true);
            expect(Value::set(null)->isInstanceOf(\stdClass::class))
                ->toBe(false);
        });
        it("is not instance of class", function () {
            expect(Value::set(new \DateTime())->isNotInstanceOf(\stdClass::class))
                ->toBe(true);
            expect(Value::set(null)->isNotInstanceOf(\stdClass::class))
                ->toBe(false);
        });
        it("is instance of class that can be null", function () {
            expect(Value::set(new \stdClass())->isInstanceNullableOf(\stdClass::class))
                ->toBe(true);
            expect(Value::set(null)->isInstanceNullableOf(\stdClass::class))
                ->toBe(true);
        });
    });
});
