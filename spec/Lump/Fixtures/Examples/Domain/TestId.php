<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain;

use Parchex\Core\Domain\Identifier;

class TestId extends Identifier
{
}
