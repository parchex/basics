<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain;

class TestValueObject
{
    /**
     * @var string
     */
    private $coin;
    /**
     * @var int
     */
    private $amount;

    public function __construct(string $coin, int $amount)
    {
        $this->coin = $coin;
        $this->amount = $amount;
    }

    public function coin(): string
    {
        return $this->coin;
    }

    public function amount(): int
    {
        return $this->amount;
    }
}
