<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain;

use Parchex\Common\ArrayCollection;
use Parchex\Common\DateTime;

class TestEntity
{
    /**
     * @var TestId
     */
    private $testId;
    /**
     * @var int
     */
    private $age;
    /**
     * @var DateTime
     */
    private $expiration;
    /**
     * @var string
     */
    private $name;
    /**
     * @var TestOtherEntity[]
     */
    private $testOtherEntities;

    public function __construct(
        TestId $testId,
        string $name,
        int $age,
        DateTime $expiration,
        array $testOtherEntities
    ) {
        $this->testId = $testId;
        $this->age = $age;
        $this->expiration = $expiration;
        $this->name = $name;
        $this->testOtherEntities = new ArrayCollection(
            $testOtherEntities,
            TestOtherEntity::class
        );
    }

    public function testId(): TestId
    {
        return $this->testId;
    }

    public function age(): int
    {
        return $this->age;
    }

    public function expiration(): DateTime
    {
        return $this->expiration;
    }

    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return TestOtherEntity[]
     */
    public function testOtherEntities(): array
    {
        return $this->testOtherEntities->toArray();
    }
}
