<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain;

class TestOtherEntity
{
    /**
     * @var TestOtherId
     */
    private $testOtherId;
    /**
     * @var string
     */
    private $description;
    /**
     * @var bool
     */
    private $activated;
    /**
     * @var int
     */
    private $level;
    /**
     * @var float
     */
    private $percent;

    public function __construct(
        TestOtherId $testOtherId,
        string $description,
        bool $activated,
        int $level,
        float $percent
    ) {
        $this->testOtherId = $testOtherId;
        $this->description = $description;
        $this->activated = $activated;
        $this->level = $level;
        $this->percent = $percent;
    }

    public function testOtherId(): TestOtherId
    {
        return $this->testOtherId;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function activated(): bool
    {
        return $this->activated;
    }

    public function level(): int
    {
        return $this->level;
    }

    public function percent(): float
    {
        return $this->percent;
    }
}
