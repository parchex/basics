<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Builders;

use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherEntity;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherId;
use Parchex\Lump\Fixtures\Charger\Value;
use Parchex\Lump\Fixtures\ObjectBuilder;

/**
 * @method \Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherEntity build()
 */
class TestOtherEntityBuilder extends ObjectBuilder
{
    public static function create()
    {
        return static::builder(
            TestOtherEntity::class,
            [TestOtherEntityProvider::class]
        );
    }

    public function withTestOtherId($testOtherId)
    {
        return $this->with(
            'testOtherId',
            Value::set($testOtherId)->identifier(TestOtherId::class)
        );
    }

    public function withDescription($description)
    {
        return $this->with(
            'description',
            Value::set((string)$description)
        );
    }

    public function withActivated($activated)
    {
        return $this->with(
            'activated',
            Value::set($activated)->boolean()
        );
    }

    public function withLevel($level)
    {
        return $this->with(
            'level',
            Value::set((int)$level)
        );
    }

    public function withPercent($percent)
    {
        return $this->with(
            'percent',
            Value::set((float)$percent)
        );
    }
}
