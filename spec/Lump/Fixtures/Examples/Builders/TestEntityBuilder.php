<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Builders;

use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestEntity;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestId;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherEntity;
use Parchex\Lump\Fixtures\Charger\Value;
use Parchex\Lump\Fixtures\ObjectBuilder;

/**
 * @method \Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestEntity build()
 */
class TestEntityBuilder extends ObjectBuilder
{
    public static function create()
    {
        return static::builder(
            TestEntity::class,
            [TestEntityProvider::class]
        );
    }

    public function withTestId($testId)
    {
        return $this->with(
            'testId',
            Value::set($testId)->identifier(TestId::class)
        );
    }

    public function withName($name)
    {
        return $this->with(
            'name',
            Value::set((string)$name)
        );
    }

    public function withAge($age)
    {
        return $this->with(
            'age',
            Value::set((int)$age)
        );
    }

    public function withExpiration($expiration)
    {
        return $this->with(
            'expiration',
            Value::set($expiration)->datetime()
        );
    }

    public function withTestOtherEntities($testOtherEntities)
    {
        return $this->with(
            'testOtherEntities',
            Value::set($testOtherEntities)
                ->collection()
                ->instance(
                    TestOtherEntity::class,
                    function ($values) {
                        return TestOtherEntityBuilder::create()
                            ->fillWith($values)
                            ->build();
                    }
                )
        );
    }
}
