<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Builders;

use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherEntity;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherId;
use Parchex\Lump\Fixtures\Faker;

class TestOtherEntityProvider extends Faker
{
    public static function testOtherEntity(): TestOtherEntity
    {
        return static::entity(TestOtherEntityBuilder::class);
    }

    public function testOtherId(): TestOtherId
    {
        return TestOtherId::fromString($this->generator->uuid);
    }

    public function description(): string
    {
        return $this->generator->text(100);
    }

    public function activated(): bool
    {
        return $this->generator->boolean;
    }

    public function level(): int
    {
        return (int)$this->generator->numberBetween(0, 100);
    }

    public function percent(): float
    {
        return (float)$this->generator->randomFloat(2);
    }
}
