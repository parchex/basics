<?php declare(strict_types=1);

namespace Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Builders;

use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestEntity;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestId;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestOtherEntity;
use Parchex\Common\DateTime;
use Parchex\Lump\Fixtures\Faker;

class TestEntityProvider extends Faker
{
    public static function testEntity(): TestEntity
    {
        return static::entity(TestEntityBuilder::class);
    }

    public function testId(): TestId
    {
        return TestId::fromString($this->generator->uuid);
    }

    public function name(): string
    {
        return $this->generator->words(3, true);
    }

    public function age(): int
    {
        return (int)$this->generator->numberBetween(18, 100);
    }

    public function expiration(): DateTime
    {
        return DateTime::fromString($this->generator->date(DateTime::FORMAT));
    }

    /**
     * @inheritDoc
     * @return TestOtherEntity[]
     */
    public static function testOtherEntities($min = 0, $max = 5): array
    {
        return static::entitiesBetween(TestOtherEntityBuilder::class, $min, $max);
    }
}
