<?php

namespace Spec\Parchex\Lump\Fixtures;

use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Builders\TestEntityBuilder;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Builders\TestOtherEntityBuilder;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestEntity;
use Fixtures\Spec\Parchex\Lump\Fixtures\Examples\Domain\TestId;
use Parchex\Common\DateTime;

describe("Object builder", function () {
    describe("creation of builder", function () {
        it("needs setup the target entity to build", function () {
            $builder = TestEntityBuilder::create();

            expect($builder->targetClass())
                ->toBe(TestEntity::class);
        });
        it("generates fixture values for entity fields", function () {
            $builder = TestEntityBuilder::create();
            $fixtures = $builder->fixtures();

            expect($fixtures)
                ->toContainKeys($builder->fields());
            foreach ($builder->fields() as $field) {
                expect($fixtures[$field])
                    ->not->toBeEmpty();
            }
        });
        it("reload fixture values generating new values each time", function () {
            $builder = TestEntityBuilder::create();
            $fixtures = $builder->fixtures();
            $newFixtures = $builder->reload()->fixtures();
            $otherFixtures = $builder->reload()->fixtures();

            expect($newFixtures)->not->toEqual($fixtures);
            expect($otherFixtures)->not->toEqual($fixtures);
        });
        it("fills with specific values for entity fields", function () {
            $builder = TestEntityBuilder::create();
            $values = [
                "testId" => TestId::fromString("111-222-333-444"),
                "name" => "Pepe",
                "age" => 23,
                "expiration" => DateTime::now(),
                "testOtherEntities" => [
                    TestOtherEntityBuilder::create()->fillWith([
                        "testOtherId" => "111-111-111",
                        "description" => "Desc Pepe",
                        "activated" => false,
                        "level" => 10,
                        "percent" => 24.6
                    ])->build(),
                    [
                        "testOtherId" => "222-222-222",
                        "description" => "Desc Juan",
                        "activated" => true,
                        "level" => 32,
                        "percent" => 12.986
                    ]
                ]
            ];
            $fixture = $builder->fillWith($values)->build();
            expect($fixture->testId())->toBe($values['testId']);
            expect($fixture->name())->toBe($values['name']);
            expect($fixture->age())->toBe($values['age']);
            expect($fixture->expiration())->toBe($values['expiration']);

            expect($fixture->testOtherEntities()[0])
                ->toBe($values['testOtherEntities'][0]);

            expect((string)$fixture->testOtherEntities()[1]->testOtherId())
                ->toBe($values['testOtherEntities'][1]['testOtherId']);
            expect($fixture->testOtherEntities()[1]->description())
                ->toBe($values['testOtherEntities'][1]['description']);
            expect($fixture->testOtherEntities()[1]->activated())
                ->toBe($values['testOtherEntities'][1]['activated']);
            expect($fixture->testOtherEntities()[1]->level())
                ->toBe($values['testOtherEntities'][1]['level']);
            expect($fixture->testOtherEntities()[1]->percent())
                ->toBeCloseTo($values['testOtherEntities'][1]['percent'], 3);
        });
    });
    describe("building entities", function () {
        it("builds a entity with fixtures generated internally", function () {
            $entity = TestEntityBuilder::create()->build();

            expect($entity->name())
                ->not->toBeEmpty();
            expect((string)$entity->testId())
                ->not->toBeEmpty();
            expect($entity->age())
                ->not->toBeEmpty();
            expect((string)$entity->expiration())
                ->not->toBeEmpty();
        });
        it("builds a different entity each time", function () {
            $builder = TestEntityBuilder::create();
            $entity = $builder->build();
            $other = $builder->build();

            expect($entity)
                ->not->toEqual($other);
        });
        it("builds a entity filling with specific values", function () {
            $entity = TestEntityBuilder::create()
                ->withTestId("111-222-333-444")
                ->withName("Pepe")
                ->withAge("32")
                ->withExpiration("2019-11-11 11:11:11")
                ->build();

            expect($entity->name())
                ->toBe("Pepe");
            expect($entity->testId())
                ->toEqual(TestId::fromString("111-222-333-444"));
            expect($entity->age())
                ->toBe(32);
            expect($entity->expiration())
                ->toEqual(DateTime::fromString("2019-11-11 11:11:11"));
        });
    });
});
