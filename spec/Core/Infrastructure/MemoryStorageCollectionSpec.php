<?php

namespace Spec\Parchex\Core\Infrastructure;

use Kahlan\Plugin\Double;
use Parchex\Common\ArrayCollection;
use Parchex\Core\Domain\Entity as EntityDomain;
use Parchex\Core\Domain\Exceptions\EntityNotFound;
use Parchex\Core\Domain\Id\StringId;
use Parchex\Core\Domain\Identifier;
use Parchex\Core\Infrastructure\InMemoryRepository;
use Parchex\Core\Infrastructure\StorageCollection;

describe("Storage for entities in memory", function () {

    describe("In memory repository", function () {
        it("storage a collection of entities with the same type", function () {
            $repository = new InMemoryRepository(
                Entity::class,
                [
                    new Entity("id-one"),
                    new Entity("id-two"),
                    new Entity("id-three"),
                    new Entity("id-four"),
                    new Entity("id-five"),
                ]
            );

            $storage = $repository->storage();
            expect($storage)
                ->toBeAnInstanceOf(StorageCollection::class);

            expect(count($storage->toArray()))
                ->toEqual(5);
        });
    });

    describe("Collection data handling", function () {

        it("can not have different types of elements specified", function () {
            $differentType = Double::instance([
                'extends' => EntityDomain::class,
                'stubMethods' => [
                    'identifier' => EntityId::fromString("od-two"),
                ],
            ]);
            $createNewStorage = function () use ($differentType) {
                new StorageCollection(
                    Entity::class,
                    [
                        new Entity("id-one"),
                        $differentType,
                        new Entity("id-three"),
                    ]
                );
            };

            expect($createNewStorage)->toThrow();
        });

        it("gets a entity by its identifier", function () {
            $expected = new Entity("id-two");

            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one"),
                    clone $expected,
                    new Entity("id-three"),
                ]
            );

            $identifier = EntityId::fromString("id-two");

            expect($storage->get($identifier))
                ->toEqual($expected);
        });

        it("is a exception when a entity cannot get", function () {
            $identifier = EntityId::fromString("id-two");
            $notFound = function () use ($identifier) {
                $storage = new StorageCollection(
                    Entity::class,
                    [
                        new Entity("id-one"),
                        new Entity("id-three"),
                    ]
                );

                $storage->get($identifier);
            };

            expect($notFound)->toThrow(new EntityNotFound($identifier));
        });

        it("removes a element given its identifier", function () {
            $identifier = "id-two";
            $removed = new Entity($identifier);

            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one"),
                    clone $removed,
                    new Entity("id-three"),
                ]
            );

            $storage->remove($removed);

            expect($storage->toArray())
                ->not->toContainKey($identifier);
        });

        it("adds new element when not exists in storage", function () {
            $identifier = "id-two";
            $entity = new Entity($identifier);

            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one"),
                    new Entity("id-three"),
                ]
            );

            $storage->set($entity);

            expect($storage->toArray())
                ->toContainKey($identifier)
                ->toContain($entity);
        });

        it("overwrites a element when set a element that exists in storage", function () {
            $identifier = "id-two";
            $entity = new Entity($identifier, "one");

            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one"),
                    new Entity("id-two", "two"),
                    new Entity("id-three"),
                ]
            );

            $storage->set($entity);

            $overwrite = $storage->get(EntityId::fromString($identifier));
            expect($overwrite)->toEqual($entity);
        });
    });

    describe("Finding entities", function () {
        it("finds entities by value in its properties", function () {
            $content = "two";
            $entity = new Entity("id-two", $content);
            $sameContentEntity = new Entity("id-three", $content);

            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one"),
                    $entity,
                    $sameContentEntity,
                ]
            );

            expect($storage->findBy("content", $content))
                ->toHaveLength(2)
                ->toContain($sameContentEntity)
                ->toContain($entity);
        });

        it("finds entities by entity in a relation with other", function () {
            $identifier = "other-eleven";
            $content = new Entity($identifier);
            $entity = new Entity("id-two", $content);

            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one", new Entity("other-ten")),
                    $entity,
                    new Entity("id-three", $content),
                ]
            );

            expect($storage->findByEntityWithIdentifier(
                "content",
                EntityId::fromString($identifier)
            ))
                ->toContainKeys("id-three")
                ->toHaveLength(2)
                ->toContain($entity);
        });

        it("finds entities by entity in a relation that contains a collection", function () {
            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one", new ArrayCollection()),
                    new Entity("id-two", new ArrayCollection([
                        new Entity("other-two"),
                        new Entity("other-three"),
                    ])),
                    new Entity("id-three", new ArrayCollection([
                        new Entity("other-one"),
                        new Entity("other-two"),
                        new Entity("other-four"),
                    ])),
                    new Entity("id-four", new ArrayCollection([
                        new Entity("other-one"),
                    ])),
                ]
            );

            expect($storage->findByEntityWithIdentifier(
                "content",
                EntityId::fromString("other-one")
            ))
                ->toHaveLength(2)
                ->toContainKeys("id-three")
                ->toContainKeys("id-four");
        });
        it("finds entities by property value of a entity relation", function () {
            $searchValue = "content to search";
            $fixture = new Entity("other-four", $searchValue);
            $storage = new StorageCollection(
                Entity::class,
                [
                    new Entity("id-one", new ArrayCollection()),
                    new Entity("id-two", new ArrayCollection([
                        new Entity("other-two"),
                        new Entity("other-three"),
                    ])),
                    new Entity("id-three", new ArrayCollection([
                        new Entity("other-one"),
                        new Entity("other-two"),
                        $fixture,
                    ])),
                    new Entity("id-four", new ArrayCollection([
                        new Entity("other-one"),
                    ])),
                ]
            );

            expect($storage->findByEntityWith(
                "content",
                "content",
                $searchValue
            ))
                ->toHaveLength(1)
                ->toContainKeys("id-three");
        });
    });

    describe("Sort collection elements", function () {
        given("storageDisordered", function () {
            return new StorageCollection(
                Entity::class,
                [
                    new Entity("id-four", 4),
                    new Entity("id-one", 1),
                    new Entity("id-five", 5),
                    new Entity("id-three", 3),
                    new Entity("id-two", 2),
                    new Entity("id-six", 6),
                ]
            );
        });

        it("sorts elements by integer property ascending", function () {
            /** @var StorageCollection $storage */
            $storage = $this->storageDisordered;
            $sort = ["content" => "ASC"];

            $before = PHP_INT_MIN;
            /** @var Entity $entity */
            foreach ($storage->orderBy($sort) as $entity) {
                expect($entity->content())
                    ->toBeGreaterThan($before);
                $before = $entity->content();
            }
        });
        it("sorts elements by integer property descending", function () {
            /** @var StorageCollection $storage */
            $storage = $this->storageDisordered;
            $sort = ["content" => "DESC"];

            $before = PHP_INT_MAX;
            /** @var Entity $entity */
            foreach ($storage->orderBy($sort) as $entity) {
                expect($entity->content())
                    ->toBeLessThan($before);
                $before = $entity->content();
            }
        });
        it("sorts elements by string property ascending", function () {
            /** @var StorageCollection $storage */
            $storage = $this->storageDisordered;
            $sort = ["identifier" => "ASC"];

            $before = '';
            /** @var Entity $entity */
            foreach ($storage->orderBy($sort) as $entity) {
                expect((string)$entity->identifier())
                    ->toBeGreaterThan($before);
                $before = (string)$entity->identifier();
            }
        });
        it("sorts elements by string property descending", function () {
            /** @var StorageCollection $storage */
            $storage = $this->storageDisordered;
            $sort = ["identifier" => "DESC"];

            $before = 'z';
            /** @var Entity $entity */
            foreach ($storage->orderBy($sort) as $entity) {
                expect((string)$entity->identifier())
                    ->toBeLessThan($before);
                $before = (string)$entity->identifier();
            }
        });
    });
});

class EntityId extends StringId
{
}

class Entity extends EntityDomain
{
    private $identifier;

    private $content;

    public function __construct(string $identifier, $content = null)
    {
        $this->identifier = EntityId::generate($identifier);
        $this->content = $content ?? bin2hex(random_bytes(10));
    }

    public function identifier(): Identifier
    {
        return $this->identifier;
    }

    public function content()
    {
        return $this->content;
    }
}
