<?php

namespace Spec\Parchex\Core\Domain;

describe("Value Objects", function () {
    it("can_be_converted_to_a_string", function() {
        $vo = new ValueObject("Pepe", 34);
        expect((string)$vo)
            ->toBe(ValueObject::class);
    });
    it("is equals to other value object when all attributes are equals", function() {
        $vo = new ValueObject("Pepe", 34);
        $other = new ValueObject("Pepe", 34);
        $different = new ValueObject("Juan", 34);

        expect($vo->equals($other))->toBeTruthy();
        expect($vo->equals($different))->toBeFalsy();
    });
    it("is equals to other value object when is the same type", function() {
        $vo = new ValueObject("Pepe", 34);
        $other = new OtherValueObject("Pepe", 34);

        expect($vo->equals($other))->toBeFalsy();
    });
});

class ValueObject extends \Parchex\Core\Domain\ValueObject
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    private $age;

    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function age(): int
    {
        return $this->age;
    }
}

class OtherValueObject extends \Parchex\Core\Domain\ValueObject {
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    private $age;

    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function age(): int
    {
        return $this->age;
    }
}
