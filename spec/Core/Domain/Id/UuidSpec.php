<?php

namespace Spec\Parchex\Core\Domain\Id;

use Parchex\Core\Domain\Identifier;

class Uuid extends \Parchex\Core\Domain\Id\Uuid
{
}

describe("UUID identifier", function () {

    it("is identifier", function () {
        expect(Uuid::fromString("id"))->toBeAnInstanceOf(Identifier::class);
    });

    it("generates non empty identifiers", function () {
        expect(Uuid::generate())->not->toBeEmpty();
    });


    it("generates UUID value", function () {
        expect((string)UuId::generate())
            ->toMatch('/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/');
    });


    it("generates different identifiers", function () {
        $first = Uuid::generate();
        $second = Uuid::generate();
        $other = Uuid::generate();

        expect($first)->not->toBe($second);
        expect($second)->not->toBe($other);
    });
});
