<?php

namespace Spec\Parchex\Core\Domain\Id;

use Parchex\Core\Domain\Identifier;

class StringId extends \Parchex\Core\Domain\Id\StringId
{
}

describe("String identifier", function () {

    it("is identifier", function () {
        expect(StringId::fromString("id"))->toBeAnInstanceOf(Identifier::class);
    });

    given("literals", function () {
        return [
            "tildes"             => [
                "idaánñoóaäid",
                "idaxnxoxaxid"
            ],
            "spaces"             => [
                "   IdAán PÑoÓaäoÒId  ",
                "idaxn-pxoxaxoxid"
            ],
            "dashes"             => [
                "-trim dashes----",
                "trim-dashes"
            ],
            "multiple dashes"    => [
                "Id-separete--With---Only----One-dash-----",
                "id-separete-with-only-one-dash"
            ],
            "strange characters" => [
                "-!\"·$%&/()=?¿\\|@#~½¬{[]}\^*Ç{}:_-><«Id6»¢¢“”µ─·{~~][þø→ø→→↓←ŧ¶€ł@æßððđđŋŋħħ̉ĸĸłł~─“¢“¢»¢«»«",
                "xx-x-x-xx-x-xxxid6xxxxxx-x-xxxxxxxx-xx-xxxx-xxxxxxxxx"
            ],
        ];
    });

    with_provided_data_it("generates a sanitized value", $this->literals, function ($string, $sanitize) {
        expect((string)StringId::generate($string))->toBe($sanitize);
    });
});
