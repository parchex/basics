<?php

namespace Spec\Parchex\Core\Domain\Id;

use Parchex\Core\Domain\Identifier;

class RandomId extends \Parchex\Core\Domain\Id\RandomId
{
}

describe("Random hexadecimal identifier", function () {

    it("is identifier", function () {
        expect(RandomId::fromString("id"))->toBeAnInstanceOf(Identifier::class);
    });

    it("generates a random hexadecimal value", function () {
        expect((string)RandomId::generate())->not->toMatch('/[^a-f0-9]+/');
    });

    it("generates values with pair length", function () {
        expect((string)RandomId::generate(8))->toHaveLength(8);
        expect((string)RandomId::generate(9))->toHaveLength(8);
    });

    it("generates values with a length minimum of " . RandomId::MIN_LENGTH, function () {
        expect((string)RandomId::generate(3))->toHaveLength(8);
    });

    it("generates different random values", function () {
        $id = RandomId::generate();
        $other = RandomId::generate();
        $more = RandomId::generate();

        expect($id)->not->toEqual($other)->not->toEqual($more);
    });
});
