<?php

namespace Spec\Parchex\Core\Domain;

use Kahlan\Arg;
use Parchex\Core\Domain\DomainEvent;
use Parchex\Core\Domain\EventPublisher;
use Parchex\Core\Domain\Identifier;
use Spec\Parchex\Core\Domain\Id\StringId;

describe("Entities", function () {
    it("can be converted to a string", function() {
        $entity = new Entity("111-222-333", "Pepe", 34);
        expect((string)$entity)
            ->toBe(Entity::class.'('.$entity->id().')');
    });
    it("is equals to other entity when identifiers are equals", function() {
        $entity = new Entity("111-222-333", "Pepe", 34);
        $other = new Entity("111-222-333", "Pepe", 34);
        $same = new Entity("111-222-333", "Juan", 34);

        expect($entity->equals($other))->toBeTruthy();
        expect($entity->equals($same))->toBeTruthy();
    });
    it("is equals to other value object when is the same type", function() {
        $entity = new Entity("111-222-333", "Pepe", 34);
        $other = new OtherEntity("111-222-333", "Pepe", 34);

        expect($entity->equals($other))->toBeFalsy();
    });
    describe("Event Domain publication", function () {
        it("publishes domain events when something relevant occurs in entity", function () {
            expect(EventPublisher::class)
                ->toReceive('::publish')
                ->with(Arg::toBeAnInstanceOf(SomethingHappenedEvent::class));

            $entity = new Entity("111-222-333", "Pepe", 34);

            $entity->actionThatPublishEvent();
        });
    });
});

class Entity extends \Parchex\Core\Domain\Entity
{
    /**
     * @var StringId
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $age;

    public function __construct(string $id, string $name, int $age)
    {
        $this->id = StringId::fromString($id);
        $this->name = $name;
        $this->age = $age;
    }

    public function identifier(): Identifier
    {
        return $this->id();
    }

    public function id(): StringId
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function age(): int
    {
        return $this->age;
    }

    public function actionThatPublishEvent()
    {
        EventPublisher::publish(new SomethingHappenedEvent());
    }
}

class OtherEntity extends \Parchex\Core\Domain\Entity
{
    /**
     * @var StringId
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $age;

    public function __construct(string $id, string $name, int $age)
    {
        $this->id = StringId::fromString($id);
        $this->name = $name;
        $this->age = $age;
    }

    public function identifier(): Identifier
    {
        return $this->id();
    }

    public function id(): StringId
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function age(): int
    {
        return $this->age;
    }
}

class SomethingHappenedEvent extends DomainEvent
{
}
