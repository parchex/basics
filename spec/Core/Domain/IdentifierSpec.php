<?php

namespace Spec\Parchex\Core\Domain;

use Kahlan\Plugin\Double;
use Parchex\Core\Domain\Identifier;

class Id extends Identifier
{
}

describe("Identifier of entities", function () {

    given("uuid", function () {

    });

    describe("factories", function () {

        it("can instance from a string value", function () {
            $id = "a-identifier";

            $uuid = Id::fromString($id);

            expect($uuid)->toBeAnInstanceOf(Identifier::class);
        });

        it("throws exception when is empty value", function () {
            expect(function () {
                Id::fromString("");
            })->toThrow(new \InvalidArgumentException());
        });
    });

    describe("parser", function () {

        it("converts to a string value", function () {
            $id = "id-in-a-string";

            $string = (string)Id::fromString($id);

            expect($string)->toBe($id);
        });
    });

    describe("equality", function () {

        it("is equal with other when has the same identifier value", function () {
            $uuid = Id::fromString("one-id");
            $equal = clone $uuid;
            $different = Id::fromString("different-id");

            expect($uuid->equals($equal))
                ->toBeTruthy();
            expect($uuid->equals($different))
                ->toBeFalsy();
        });

        it("is equal with other when are the same type", function () {
            /** @var Id $oneUuidType */
            $oneUuidType = Double::classname([
                'class'   => 'OneUuid',
                'extends' => Id::class
            ]);
            /** @var Id $otherUuidType */
            $otherUuidType = Double::classname([
                'class'   => 'OtherUuid',
                'extends' => Id::class
            ]);

            $oneId = $oneUuidType::fromString("same-id");
            $sameTypeId = $oneUuidType::fromString("same-id");
            $otherId = $otherUuidType::fromString("same-id");

            expect($oneId->equals($sameTypeId))->toBeTruthy();
            expect($oneId->equals($otherId))->toBeFalsy();
            expect($oneId)->toEqual($sameTypeId);
            expect($oneId)->not->toEqual($otherId);
        });
    });
});
