<?php

namespace Spec\Parchex\Core\Domain;

use Kahlan\Plugin\Double;
use Parchex\Core\Domain\DomainEvent;
use Parchex\Lump\Events\Event;

describe("Event domain", function () {
    it("is a event", function () {
        $eventDomain = Double::instance(['extends' => DomainEvent::class]);

        expect($eventDomain)->toBeAnInstanceOf(Event::class);
    });
});
