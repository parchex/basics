<?php

namespace Spec\Parchex\Core\Application\Response;

use Parchex\Common\ContentImmutable;
use Parchex\Core\Application\Response\Item;
use Parchex\Core\Application\Response\ListItems;
use Parchex\Core\Application\Response\Pagination;
use Parchex\Core\Application\Response\Response;

describe("Responses", function () {
    describe("Response data structure", function () {

        given("response", function () {
            return new class([
                'field' => 'value',
                'other' => 'different'
            ]) extends Response
            {
            };
        });

        it("implements content immutable behaviour", function () {
            expect($this->response)->toBeAnInstanceOf(ContentImmutable::class);
        });

        it("is accessible to properties", function () {
            expect($this->response)
                ->toBeAnInstanceOf(\ArrayAccess::class);
            expect($this->response['field'])->toBe("value");
            expect($this->response['other'])->toBe("different");
        });

        it("is serializable to json format", function () {
            expect($this->response)
                ->toBeAnInstanceOf(\JsonSerializable::class);
            expect(json_encode($this->response))->toBe('{"field":"value","other":"different"}');
        });
    });

    describe("Response data for a item element", function () {

        given("response", function () {
            return new Item([
                'field' => 'value',
                'other' => 'different'
            ]);
        });

        it("is a response", function () {
            expect($this->response)->toBeAnInstanceOf(Response::class);
        });
    });

    describe("Response for a list of items", function () {

        given('response', function () {
            return new ListItems(
                [
                    ['field' => 'value', 'other' => 'different'],
                    ['field' => 'more', 'other' => 'less'],
                ],
                'tests',
                new Pagination(4, 23)
            );
        });

        it("is a response", function () {
            expect($this->response)->toBeAnInstanceOf(Response::class);
        });

        it("defines a named type access for items", function () {
            expect($this->response)->toContainKey('tests');
        });

        it("lists items when access by key type name", function () {
            expect($this->response['tests'])
                ->toHaveLength(2)
                ->toContain(['field' => 'value', 'other' => 'different'])
                ->toContain(['field' => 'more', 'other' => 'less']);
        });

        it("contains info about pagination", function () {
            expect($this->response['page'])->toBeAnInstanceOf(Response::class);
            expect($this->response['page']['total_items'])->toBe(23);
            expect($this->response['page']['total_pages'])->toBe(4);
        });
    });
});
