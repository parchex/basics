<?php declare(strict_types=1);

namespace Tests\Parchex\PHPUnit\TestCases;

use Parchex\Common\Func;
use Parchex\Core\Domain\Entity;
use Parchex\Core\Domain\Identifier;
use Parchex\Core\Domain\ValueObject;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\Callback;

trait CollectionTestCase
{
    public static function assertCollectionContainsAllItems($items, $collection, $message = ""): void
    {
        Assert::assertSameSize($items, $collection, $message);
        foreach ($items as $item) {
            static::assertCollectionContainsItem($item, $collection, $message);
        }
    }

    public static function assertCollectionContainsItem($item, $collection, $message = ""): void
    {
        if (static::collectionItemsAreEntitiesOrValueObjects($item, $collection)) {
            Assert::assertThat(
                $item,
                new Callback(
                    function ($value) use ($collection) {
                        foreach ($collection as $entity) {
                            if ($value->equals($entity)) {
                                return true;
                            }
                        }

                        return false;
                    }
                ),
                "Item " . get_class($item) . "('" . var_export($item, true)
                . "') is not founded in collection. " . $message
            );
        } else {
            Assert::assertContains($item, $collection, $message);
        }
    }

    public static function assertCollectionNotContainsAnyItems($items, $collection, $message = ""): void
    {
        foreach ($items as $item) {
            static::assertCollectionNotContainsItem($item, $collection, $message);
        }
    }

    public static function assertCollectionNotContainsItem($item, $collection, $message = ""): void
    {
        if (static::collectionItemsAreEntitiesOrValueObjects($item, $collection)) {
            Assert::assertThat(
                $item,
                new Callback(
                    function ($value) use ($collection) {
                        foreach ($collection as $entity) {
                            if ($value->equals($entity)) {
                                return false;
                            }
                        }

                        return true;
                    }
                ),
                "Item " . var_export($item, true) . "' is founded in collection. " . $message
            );
        } else {
            Assert::assertNotContains($item, $collection, $message);
        }
    }

    /**
     * @inheritDoc
     * @return Identifier[]
     */
    public static function getIdentifiers($collection): array
    {
        return Func::map(
            function (Entity $entity) {
                return $entity->identifier();
            },
            $collection
        );
    }

    private static function collectionItemsAreEntitiesOrValueObjects($item, $collection): bool
    {
        $isComparable = is_object($item) &&
            (is_a($item, Entity::class) || is_a($item, ValueObject::class));
        if (!$isComparable) {
            return false;
        }

        $itemClass = get_class($item);

        return array_reduce(
            (array) $collection,
            function ($isComparable, $item) use ($itemClass) {
                return $isComparable && ($item instanceof $itemClass);
            },
            true
        );
    }
}
