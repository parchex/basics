<?php declare(strict_types=1);

namespace Tests\Parchex\PHPUnit\TestCases;

use Parchex\Core\Domain\EventManager;
use Parchex\Lump\Events\AbstractListener;
use Parchex\Lump\Events\Event;

/**
 * Spy to listen events emitted
 */
class EventListenerSpy extends AbstractListener
{
    /**
     * @var Event[]
     */
    private $events;

    public function __construct()
    {
        EventManager::subscriber()->attach('*', $this);
    }

    public function handle($event): void
    {
        $this->events[] = $event;
    }

    /**
     * @return Event[]
     */
    public function eventsPublished(): array
    {
        return $this->events;
    }
}
