<?php declare(strict_types=1);

namespace Tests\Parchex\PHPUnit\TestCases;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\Callback;

trait EventTestCase
{
    /**
     * @var EventListenerSpy
     */
    protected static $listenerSpy;

    protected function setUp(): void
    {
        parent::setUp();

        static::initEventListenerSpy();
    }

    public static function assertEventIsSubmitted(
        string $eventClass,
        int $position = null
    ): void {
        $events = static::$listenerSpy->eventsPublished();
        if (count($events) === 1) {
            $position = 0;
        }

        Assert::assertNotEmpty($events, 'Event ' . $eventClass . ' is not emitted');

        if (isset($position)) {
            Assert::assertArrayHasKey(
                $position,
                $events,
                'Event ' . $eventClass . ' is not emitted in position $position'
            );
            Assert::assertInstanceOf(
                $eventClass,
                $events[$position],
                'Event ' . $eventClass . ' is not emitted in position $position'
            );
        } else {
            Assert::assertThat(
                $eventClass,
                new Callback(
                    function ($class) use ($events) {
                        foreach ($events as $event) {
                            if ($event instanceof $class) {
                                return true;
                            }
                        }

                        return false;
                    }
                ),
                'Event ' . $eventClass . ' is not emitted'
            );
        }
    }

    protected static function initEventListenerSpy(): void
    {
        static::$listenerSpy = new EventListenerSpy();
    }
}
