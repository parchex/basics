<?php declare(strict_types=1);

namespace Parchex\Core\Domain\Exceptions;

use DomainException;
use Parchex\Core\Domain\Identifier;

class EntityNotFound extends DomainException
{
    /**
     * {@inheritdoc}
     */
    public function __construct(Identifier $entityId, string $entity = null)
    {
        parent::__construct(
            'Entity ' . $entity . (isset($entity) ? ' ' : '') . 'with ' .
            get_class($entityId) . '("' . (string) $entityId . '") not exists in the repository'
        );
    }
}
