<?php declare(strict_types=1);

namespace Parchex\Core\Domain;

final class EventPublisher
{
    public static function publish(DomainEvent $eventDomain): void
    {
        EventManager::publisher()->trigger($eventDomain);
    }
}
