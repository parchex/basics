<?php declare(strict_types=1);

namespace Parchex\Core\Domain;

use Parchex\Lump\Events\Generator;

trait EventGenerator
{
    use Generator;

    protected function record(DomainEvent $event): self
    {
        $this->record($event);

        return $this;
    }
}
