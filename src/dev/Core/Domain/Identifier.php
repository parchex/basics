<?php declare(strict_types=1);

namespace Parchex\Core\Domain;

use InvalidArgumentException;

abstract class Identifier extends ValueObject
{
    /**
     * @var string
     */
    private $identity;

    final private function __construct(string $identity)
    {
        if (trim($identity) === '') {
            throw new InvalidArgumentException(
                'Identifier' . static::class . ' cannot be empty'
            );
        }

        $this->identity = $identity;
    }

    /**
     * Create a identifier from other value
     * that have a string representation
     *
     * {@inheritdoc}
     *
     * @return static
     */
    public static function fromString(string $value): Identifier
    {
        return new static($value);
    }

    /**
     * Convert identifier to a string representation
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->identity;
    }
}
