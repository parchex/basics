<?php declare(strict_types=1);

namespace Parchex\Core\Domain\Id;

use Exception;
use Parchex\Core\Domain\Identifier;

abstract class Uuid extends Identifier
{
    /**
     * {@inheritdoc}
     *
     * @return static
     *
     * @throws Exception
     */
    public static function generate(): Uuid
    {
        return static::fromString(\Ramsey\Uuid\Uuid::uuid4()->toString());
    }
}
