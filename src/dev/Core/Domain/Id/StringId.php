<?php declare(strict_types=1);

namespace Parchex\Core\Domain\Id;

use Parchex\Core\Domain\Identifier;

abstract class StringId extends Identifier
{
    /**
     * {@inheritdoc}
     *
     * @return static
     */
    public static function generate(string $string): StringId
    {
        return static::fromString(static::sanitize($string));
    }

    private static function sanitize(string $string): string
    {
        return mb_strtolower(
            self::replaceNonAlphabetCodesWithSlash(
                self::replaceNonAsciiCodesWithHexCode(
                    self::sanitizeFullSpecialChars($string)
                )
            )
        );
    }

    private static function replaceNonAlphabetCodesWithSlash(string $string): string
    {
        return trim(
            (string) mb_ereg_replace(
                '[^a-zA-Z0-9]+',
                '-',
                $string
            ),
            ' -'
        );
    }

    private static function replaceNonAsciiCodesWithHexCode(string $string): string
    {
        return (string) mb_ereg_replace(
            '&[#]?[A-z0-9]+;',
            'x',
            $string
        );
    }

    private static function sanitizeFullSpecialChars(string $string): string
    {
        return (string) filter_var(
            $string,
            FILTER_SANITIZE_FULL_SPECIAL_CHARS,
            FILTER_FLAG_STRIP_LOW | FILTER_FLAG_ENCODE_AMP
            | FILTER_FLAG_ENCODE_HIGH
        );
    }
}
