<?php declare(strict_types=1);

namespace Parchex\Core\Domain\Id;

use Exception;
use Parchex\Core\Domain\Identifier;

abstract class RandomId extends Identifier
{
    public const MIN_LENGTH = 8;

    /**
     * {@inheritdoc}
     *
     * @return static
     *
     * @throws Exception
     */
    public static function generate(int $length = 8): RandomId
    {
        $length = $length < RandomId::MIN_LENGTH ? RandomId::MIN_LENGTH : $length;

        return static::fromString(
            bin2hex(random_bytes((int) (($length - ($length % 2)) / 2)))
        );
    }
}
