<?php declare(strict_types=1);

namespace Parchex\Core\Domain;

abstract class Entity
{
    abstract public function identifier(): Identifier;

    public function equals(Entity $entity): bool
    {
        return get_class($entity) === static::class
            && $this->identifier()->equals($entity->identifier());
    }

    public function __toString(): string
    {
        return static::class . '(' . $this->identifier() . ')';
    }
}
