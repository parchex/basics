<?php declare(strict_types=1);

namespace Parchex\Core\Domain;

abstract class ValueObject
{
    public function equals(ValueObject $other): bool
    {
        return get_class($other) === static::class
            && $this == $other;
    }

    public function __toString(): string
    {
        return static::class;
    }
}
