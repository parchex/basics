<?php declare(strict_types=1);

namespace Parchex\Core\Domain;

use Parchex\Lump\Events\AbstractEvent;

/**
 * Generic Event class for define domain events
 */
abstract class DomainEvent extends AbstractEvent
{
}
