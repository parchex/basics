<?php declare(strict_types=1);

namespace Parchex\Core\Infrastructure;

use ArrayIterator;
use Closure;
use Exception;
use Parchex\Common\ArrayCollection;
use Parchex\Common\Assertion;
use Parchex\Core\Domain\Entity;
use Parchex\Core\Domain\Exceptions\EntityNotFound;
use Parchex\Core\Domain\Identifier;
use ReflectionClass;
use ReflectionException;
use Traversable;

class StorageCollection
{
    /**
     * @var array<ReflectionClass>
     */
    protected static $entityClass = [];
    /**
     * @var ArrayCollection<string, Entity>
     */
    private $collection;

    /**
     * {@inheritdoc}
     *
     * @param class-string $entityClass
     * @param array<Entity> $elements
     *
     * @throws ReflectionException
     */
    public function __construct(string $entityClass, array $elements = [])
    {
        Assertion::allIsItemOfCollection($elements, $entityClass);

        static::$entityClass[$entityClass] = new ReflectionClass($entityClass);

        $this->collection = new ArrayCollection();
        foreach ($elements as $entity) {
            $this->collection[(string) $entity->identifier()] = $entity;
        }
    }

    /**
     * @return Traversable<Entity>
     *
     * @throws Exception
     */
    public function getIterator(): Traversable
    {
        return $this->collection->getIterator();
    }

    /**
     * @return array<object>
     */
    public function toArray(): array
    {
        return $this->collection->toArray();
    }

    /**
     * {@inheritdoc}
     *
     * @return Entity|mixed
     */
    public function get(Identifier $entityId)
    {
        if (!isset($this->collection[(string) $entityId])) {
            throw new EntityNotFound($entityId);
        }

        return $this->collection[(string) $entityId];
    }

    public function remove(Entity $entity): void
    {
        unset($this->collection[(string) $entity->identifier()]);
    }

    public function set(Entity $entity): void
    {
        $this->collection[(string) $entity->identifier()] = $entity;
    }

    /**
     * List objects can be sorted with specific sort params...
     * <code>
     * [
     * [fieldName, asc|desc]
     * ...
     * ]
     * </code>
     * Example...
     * <code>
     * $sort = [
     *   'group' => 'ASC',
     *   'title' => 'DESC'
     * ]
     * </code>
     * {@inheritdoc}
     *
     * @param array<string, string> $sort
     *
     * @return array<Entity>
     *
     * @throws Exception
     */
    public function orderBy(array $sort): iterable
    {
        /** @var ArrayIterator $iterator */
        $iterator = $this->collection->getIterator();

        $iterator->uasort(
            static function ($elemA, $elemB) use ($sort) {
                return array_reduce(
                    array_keys($sort),
                    static function ($comp, $byField) use ($elemA, $elemB, $sort) {
                        if ($comp !== 0) {
                            return $comp;
                        }
                        $order = $sort[$byField];

                        return (($elemA ? $elemA->$byField() : '')
                                <=>
                                ($elemB ? $elemB->$byField() : ''))
                            * ($order !== 'DESC' ? 1 : -1);
                    },
                    0
                );
            }
        );

        return $iterator;
    }

    /**
     * Find all entities in collection that have this property with value specified
     * {@inheritdoc}
     *
     * @param mixed $value
     *
     * @return array<Entity>
     */
    public function findBy(string $property, $value, bool $strict = true): iterable
    {
        return $this->filterBy(
            static function ($val) use ($value, $strict): bool {
                return $strict
                    ? ($val === $value)
                    : ($val == $value);
            },
            [$property]
        );
    }

    /**
     * Find all entities in collection that have a relation whose
     * entity will be equals by identifier specified
     * When the relation is a collection will search into elements
     * that have that identity.
     * {@inheritdoc}
     *
     * @return array<Entity>
     */
    public function findByEntityWithIdentifier(
        string $entityName,
        Identifier $entityId
    ): iterable {
        return $this->filterBy(
            static function (Entity $entity) use ($entityId): bool {
                return $entity->identifier()->equals($entityId);
            },
            [$entityName]
        );
    }

    /**
     * Find all entities in collection that have a relation whose
     * entity with a property that will be equals to value specified
     * When the relation is a collection will search into elements
     * that have that identity.
     * {@inheritdoc}
     *
     * @param mixed $value
     *
     * @return array<Entity>
     */
    public function findByEntityWith(
        string $entityName,
        string $property,
        $value
    ): iterable {
        return $this->filterBy(
            function (Entity $entity) use ($property, $value): bool {
                return $this->getAttributeValue($entity, $property) === $value;
            },
            [$entityName]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @param array<string> $relations
     *
     * @return array<Entity>
     */
    protected function filterBy(Closure $condition, array $relations = []): iterable
    {
        return $this->collection->filter(
            function (Entity $entity) use ($condition, $relations): bool {
                return $this->exists($entity, $condition, $relations);
            }
        );
    }

    /**
     * Check if a value exist in some relations of entity.
     * When a relation is a collection, search into all relations of all entities of collection
     *
     * {@inheritdoc}
     *
     * @param mixed|ArrayCollection $attributeValue
     * @param array<string> $relations Entity names
     *
     * @throws ReflectionException
     */
    protected function exists($attributeValue, Closure $condition, array $relations = []): bool
    {
        // If mo more relations in entity apply condition
        if (count($relations) === 0) {
            $fnc = static function ($entity) use ($condition) {
                return $condition($entity);
            };
        } else {
            /**
             * When is not collection already can check value from attribute
             */
            if (!($attributeValue instanceof ArrayCollection)) {
                $attributeValue = $this->getAttributeValue(
                    $attributeValue,
                    array_shift($relations)
                );
            }
            $fnc = function ($entity) use ($condition, $relations): bool {
                return $this->exists($entity, $condition, $relations);
            };
        }

        if (($attributeValue instanceof ArrayCollection)) {
            return $attributeValue->exists(
                static function ($key, $entity) use ($fnc) {
                    return $fnc($entity);
                }
            );
        }

        return $fnc($attributeValue);
    }

    /**
     * {@inheritdoc}
     *
     * @return object|mixed
     *
     * @throws ReflectionException
     */
    protected function getAttributeValue(object $entity, string $attrName)
    {
        $entityClass = static::$entityClass[get_class($entity)]
            ?? new ReflectionClass(get_class($entity));

        if ($entityClass->hasMethod($attrName)) {
            return $entity->$attrName();
        }

        $property = $entityClass->getProperty($attrName);
        $property->setAccessible(true);

        return $property->getValue($entity);
    }
}
