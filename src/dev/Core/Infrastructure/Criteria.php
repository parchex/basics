<?php declare(strict_types=1);

namespace Parchex\Core\Infrastructure;

use Closure;

class Criteria
{
    public const ASC = 'ASC';
    public const DESC = 'DESC';
    /**
     * @var Closure|null
     */
    private $filterExpression;
    /**
     * @var array<string>
     */
    private $orderings = [];
    /**
     * @var int
     */
    private $firstResult;
    /**
     * @var int|null
     */
    private $maxResults;

    /**
     * "{@inheritdoc}"
     *
     * @param array<string> $orderings
     */
    protected function __construct(
        ?Closure $filterExpression = null,
        ?array $orderings = [],
        ?int $firstResult = null,
        ?int $maxResults = null
    ) {
        $this->filterExpression = $filterExpression;

        $this->firstResult = $firstResult ?? 0;
        $this->maxResults = $maxResults;

        $this->withOrderings($orderings ?? []);
    }

    public static function create(): self
    {
        return new self();
    }

    /**
     * Sets the ordering of the result of this Criteria.
     * Keys are field and values are the order, being either ASC or DESC.
     *
     * {@inheritdoc}
     *
     * @param array<string> $orderings
     */
    public function withOrderings(array $orderings): self
    {
        $this->orderings = array_map(
            static function (string $ordering): string {
                return strtoupper($ordering) === Criteria::DESC ? Criteria::DESC : Criteria::ASC;
            },
            $orderings
        );

        return $this;
    }

    public function withMaxResults(int $maxResults): self
    {
        $this->maxResults = $maxResults;

        return $this;
    }

    public function withFilterExpression(Closure $filterExpression): self
    {
        $this->filterExpression = $filterExpression;

        return $this;
    }

    public function withFirstResult(int $firstResult): self
    {
        $this->firstResult = $firstResult;

        return $this;
    }

    public function filterExpression(): ?Closure
    {
        return $this->filterExpression;
    }

    /**
     * @return array<string>
     */
    public function orderings(): array
    {
        return $this->orderings;
    }

    public function firstResult(): int
    {
        return $this->firstResult;
    }

    public function maxResults(): ?int
    {
        return $this->maxResults;
    }
}
