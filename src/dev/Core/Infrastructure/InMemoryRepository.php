<?php declare(strict_types=1);

namespace Parchex\Core\Infrastructure;

use Parchex\Core\Domain\Entity;
use ReflectionException;

/**
 * Base class for implement repositories for testing proposes
 * and can store entities in memory, and simulate a persistence
 */
class InMemoryRepository
{
    /**
     * @var StorageCollection
     */
    protected $storage;

    /**
     * {@inheritdoc}
     *
     * @param class-string $entityClass
     * @param array<Entity> $entities
     *
     * @throws ReflectionException
     */
    public function __construct(string $entityClass, array $entities = [])
    {
        $this->storage = new StorageCollection($entityClass, $entities);
    }

    public function storage(): StorageCollection
    {
        return $this->storage;
    }
}
