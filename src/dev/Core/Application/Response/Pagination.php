<?php declare(strict_types=1);

namespace Parchex\Core\Application\Response;

class Pagination extends Response
{
    public function __construct(int $totalPages, int $totalItems = null)
    {
        parent::__construct(
            [
                'total_items' => $totalItems,
                'total_pages' => $totalPages,
            ]
        );
    }
}
