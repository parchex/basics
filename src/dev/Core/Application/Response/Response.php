<?php declare(strict_types=1);

namespace Parchex\Core\Application\Response;

use Parchex\Common\ContentImmutable;
use Parchex\Common\ContentImmutableTrait;

abstract class Response implements ContentImmutable
{
    use ContentImmutableTrait;
}
