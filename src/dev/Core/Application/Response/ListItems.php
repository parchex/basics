<?php declare(strict_types=1);

namespace Parchex\Core\Application\Response;

class ListItems extends Response
{
    /**
     * @var string
     */
    protected $itemType;

    /**
     * @param array<Item> $items
     */
    public function __construct(
        array $items,
        string $itemType = 'items',
        Pagination $pagination = null
    ) {
        $this->itemType = $itemType;

        parent::__construct(
            [
                $this->itemType() => $items,
            ]
        );

        if (isset($pagination)) {
            $this->prop['page'] = $pagination;
        }
    }

    /**
     * @return array<Item>
     */
    public function items(): array
    {
        return $this->prop[$this->itemType()];
    }

    public function totalItems(): int
    {
        return $this->prop['total_items'];
    }

    public function totalPages(): int
    {
        return $this->prop['total_pages'];
    }

    protected function itemType(): string
    {
        return $this->itemType;
    }
}
