<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures;

use Faker\Provider\Base;

class Faker extends Base
{
    /**
     * Return a random numbers of random elements
     * {@inheritdoc}
     *
     * @param array<mixed> $array elements to choice
     * @param int $min minimum elements to choice (max. is length array)
     *
     * @return array<mixed>
     */
    public static function randomElementsBetween(
        array $array,
        int $min = 1,
        bool $allowDuplicates = false
    ): array {
        return parent::randomElements(
            $array,
            static::numberBetween(min($min, count($array)), count($array)),
            $allowDuplicates
        );
    }

    /**
     * Build a random number of entities
     * {@inheritdoc}
     *
     * @param class-string<ObjectBuilder>|ObjectBuilder $builderClass Entity builder
     *
     * @return array<mixed>
     */
    public static function entitiesBetween(
        $builderClass,
        int $min = 1,
        int $max = 5
    ): array {
        return array_map(
            static function ($builder) {
                return static::entity($builder);
            },
            array_fill(
                0,
                max(1, static::numberBetween($min, $max)),
                $builderClass
            )
        );
    }

    /**
     * {@inheritdoc}
     *
     * @param class-string<ObjectBuilder>|ObjectBuilder $builderClass
     */
    public static function entity($builderClass): object
    {
        $builder = is_a($builderClass, ObjectBuilder::class)
            ? $builderClass
            : $builderClass::create();

        return $builder->build();
    }

    /**
     * {@inheritdoc}
     *
     * @param class-string<ObjectBuilder>|ObjectBuilder $builderClass
     *
     * @return array<mixed>
     */
    public static function entitiesTotal($builderClass, int $count = 5): array
    {
        $entities = [];
        for ($num = max(1, $count); $num--;) {
            $entities[] = static::entity($builderClass);
        }

        return $entities;
    }
}
