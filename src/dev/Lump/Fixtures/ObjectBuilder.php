<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures;

use Faker\Factory;
use LogicException;
use Parchex\Lump\Fixtures\Charger\Filler;
use Parchex\Lump\Fixtures\Charger\Value;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;

/**
 * Helper to build model objects to generate fixtures with fake data.
 */
abstract class ObjectBuilder
{
    /**
     * @var array<ObjectBuilder>
     */
    protected static $builders;
    /**
     * @var ReflectionClass
     */
    protected $targetClass;
    /**
     * @var ReflectionMethod
     */
    protected $constructor;
    /**
     * List of names of parameters in class constructor
     *
     * @var array<string>
     */
    protected $fields;
    /**
     * @var Filler
     */
    protected $filler;

    /**
     * {@inheritdoc}
     *
     * @param class-string $targetClass
     * @param array<class-string<Faker>> $fakeProviderClasses Class names for fakers
     *
     * @throws ReflectionException
     */
    final private function __construct(
        string $targetClass,
        array $fakeProviderClasses = []
    ) {
        $this->targetClass = new ReflectionClass($targetClass);

        $constructor = $this->targetClass->getConstructor();
        if (is_null($constructor)) {
            throw new LogicException(
                $this->targetClass->getNamespaceName() . ' without constructor'
            );
        }
        $this->initConstructClass($constructor);
        $this->initFaker($fakeProviderClasses);
    }

    /**
     * Method to implement into specific class builder to invoke
     * singleton builder instantiation with params for config class
     * to instance and data fake providers to generate content for
     * properties.
     *
     * <code>
     *  public static function create()
     *  {
     *    return static::builder(Tag::class, [TagProvider::class]);
     *  }
     * </code>
     *
     * @return static
     */
    abstract public static function create();

    /**
     * @return array<string>
     */
    public function fields(): array
    {
        return $this->fields;
    }

    public function targetClass(): string
    {
        return $this->targetClass->getName();
    }

    /**
     * @return array<object>
     */
    public function fixtures(): array
    {
        return $this->filler->toArray();
    }

    /**
     * @return static
     */
    public function reload(): self
    {
        $this->filler->refill();

        return $this;
    }

    /**
     * Fill fields with values specified in array data
     *
     * <code>
     * $builder->fillWith([
     *   'propName' => 'Value Property',
     *   'otherProperty' => 'Other value'
     * ]);
     * <code>
     *
     * {@inheritdoc}
     *
     * @param array<mixed> $fieldValues
     */
    public function fillWith(array $fieldValues): self
    {
        foreach ($fieldValues as $field => $value) {
            $method = 'with' . $field;
            method_exists($this, $method)
                ?
                $this->$method($value)
                :
                $this->filler->withValue($field, $value);
        }

        return $this;
    }

    /**
     * Instance object with data generated o assigned
     *
     * @throws ReflectionException
     */
    public function build(): object
    {
        $instance = $this->targetClass->newInstanceWithoutConstructor();
        $this->constructor
            ->invokeArgs(
                $instance,
                array_map(
                    [$this, 'makeParamValue'],
                    $this->constructor->getParameters()
                )
            );

        $this->reload();

        return $instance;
    }

    /**
     * Create singleton builder for a class, with data fake providers
     *
     * {@inheritdoc}
     *
     * @param class-string $targetClass
     * @param array<class-string<Faker>> $fakeProviderClasses Class names for fakers
     *
     * @throws ReflectionException
     */
    protected static function builder(
        string $targetClass,
        array $fakeProviderClasses = []
    ): self {
        static::$builders[static::class]
            = static::$builders[static::class]
            ?? new static($targetClass, $fakeProviderClasses);

        return static::$builders[static::class];
    }

    protected function with(string $field, Value $value): self
    {
        $this->filler->with($field, $value);

        return $this;
    }

    /**
     * Initialization information for class to build
     *
     * {@inheritdoc}
     */
    private function initConstructClass(ReflectionMethod $constructor): void
    {
        $this->constructor = $constructor;

        $this->constructor->setAccessible(true);

        $this->fields = array_map(
            static function (ReflectionParameter $param): string {
                return $param->getName();
            },
            $this->constructor->getParameters()
        );
    }

    /**
     * Initialize Faker with new providers
     *
     * {@inheritdoc}
     *
     * @param array<class-string<Faker>> $fakeProviderClasses Class names for fakers
     */
    private function initFaker(array $fakeProviderClasses): void
    {
        $generator = Factory::create();
        foreach ($fakeProviderClasses as $fakeProviderClass) {
            $generator->addProvider(new $fakeProviderClass($generator));
        }

        $this->filler = new Filler($this->fields, $generator);
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed|null
     */
    private function makeParamValue(ReflectionParameter $param)
    {
        $value = $this->filler->valueOf($param->getName());

        /** @var ?ReflectionNamedType $type */
        $type = $param->getType();
        if (is_null($type)) {
            return $value->get();
        }
        if ($type->allowsNull() && $value->isNull()) {
            return null;
        }
        if ($type->getName() === 'bool') {
            return $value->boolean()->get();
        }

        return $value->get();
    }
}
