<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Command;

use ReflectionClass;

class FixtureNamespace
{
    /**
     * @var string
     */
    private $nsPrefix;
    /**
     * @var string
     */
    private $nsSuffix;

    public function __construct(string $nsPrefix, string $nsSuffix)
    {
        $this->nsPrefix = trim($nsPrefix, '\\');
        $this->nsSuffix = trim($nsSuffix, '\\');
    }

    public function generateNamespace(ReflectionClass $class): string
    {
        $builderNamespace = trim(
            str_replace(
                '\\Domain\\',
                '\\',
                '\\' . $class->getNamespaceName() . '\\'
            ),
            '\\'
        );

        return trim($this->nsPrefix . '\\' . $builderNamespace . '\\' . $this->nsSuffix, '\\');
    }

    public function generateClassnameBuilder(ReflectionClass $class): string
    {
        return $class->getShortName() . "Builder";
    }

    public function generateClassnameProvider(ReflectionClass $class): string
    {
        return $class->getShortName() . "Provider";
    }
}
