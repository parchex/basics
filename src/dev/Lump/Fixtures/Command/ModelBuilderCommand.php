<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Command;

use Composer\Autoload\ClassLoader;
use ReflectionException;
use RuntimeException;
use Symfony\Component\Console\Output\OutputInterface;

class ModelBuilderCommand
{
    /**
     * @var ClassLoader
     */
    private $classLoader;

    public function __construct(ClassLoader $classLoader)
    {
        $this->classLoader = $classLoader;
    }

    protected function createSourceCodeFile(
        ClassCodeGenerator $codeGenerator,
        ReflectionDomainClass $targetClass,
        bool $overwrite
    ): void {
        $classCode = $codeGenerator->generateClassCode($targetClass);

        $builderPath = $this->generatePathForNamespace($classCode['namespace']);
        $builderFileName = $classCode['classname'] . '.php';
        if (!$overwrite && file_exists($builderPath . '/' . $builderFileName)) {
            throw new RuntimeException(
                "Already exists class builder '${classCode['namespace']}\\${classCode['classname']}'"
            );
        }

        if (!file_exists($builderPath)) {
            mkdir($builderPath, 0775, true);
        }
        file_put_contents($builderPath . '/' . $builderFileName, $classCode['code']);
    }

    protected function generatePathForNamespace(string $builderNamespace): string
    {
        $builderPath = "";
        $builderNamespace .= '\\';
        $lenNamespace = strlen($builderNamespace);

        $prefixes = $this->classLoader->getPrefixesPsr4();
        while ($builderPath === "" && current($prefixes) !== false) {
            $prefix = key($prefixes);
            if ($this->namespaceStartWithPrefix($builderNamespace, $prefix, $lenNamespace)) {
                $prefixPath = current($prefixes)[0];
                $this->makePathsForPrefixNamespace($prefixPath);
                $builderPath = realpath($prefixPath) . '/' .
                    str_replace('\\', '/', substr($builderNamespace, strlen($prefix)));
            }
            next($prefixes);
        }

        return $builderPath;
    }

    private function namespaceStartWithPrefix(
        string $builderNamespace,
        string $prefix,
        int $lenNamespace
    ): bool {
        $lenPrefix = strlen($prefix);

        return $lenNamespace >= $lenPrefix &&
            substr_count($builderNamespace, $prefix, 0, strlen($prefix)) > 0;
    }

    private function makePathsForPrefixNamespace(string $prefixPath): void
    {
        if (!file_exists($prefixPath)) {
            mkdir($prefixPath, 0775, true);
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param array<class-string> $class
     *
     * @throws ReflectionException
     */
    public function __invoke(
        array $class,
        string $nsPrefix,
        string $nsSuffix,
        bool $overwrite,
        OutputInterface $output
    ): void {
        $nsFixture = new FixtureNamespace($nsPrefix, $nsSuffix);
        $builderGenerator = new ObjectBuilderCodeCreator($nsFixture, $output);
        $providerGenerator = new DataProviderCodeCreator($nsFixture, $output);

        foreach ($class as $className) {
            $targetClass = new ReflectionDomainClass($className);
            $output->writeln(
                "<question>Creating fixture tools for class...</question> " .
                $targetClass->getName() . ""
            );

            $this->createSourceCodeFile(
                $builderGenerator,
                $targetClass,
                $overwrite
            );
            $this->createSourceCodeFile(
                $providerGenerator,
                $targetClass,
                $overwrite
            );
        }
    }
}
