<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Command;

use LogicException;
use Parchex\Common\DateTime;
use Parchex\Core\Domain\Identifier;
use ReflectionClass;
use ReflectionNamedType;
use ReflectionParameter;
use stdClass;
use Symfony\Component\Console\Output\OutputInterface;

class ObjectBuilderCodeCreator implements ClassCodeGenerator
{
    /**
     * @var FixtureNamespace
     */
    private $nsFixture;
    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(FixtureNamespace $nsFixture, OutputInterface $output)
    {
        $this->nsFixture = $nsFixture;
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     *
     * @return array<mixed>
     */
    public function generateClassCode(ReflectionDomainClass $targetClass): array
    {
        $namespace = $this->nsFixture->generateNamespace($targetClass);
        $classBuilder = $this->nsFixture->generateClassnameBuilder($targetClass);

        $this->output->writeln(
            "<info>  Generating class for fixture builder...</info> " .
            $namespace . '\\' . $classBuilder
        );

        [$fieldMethods, $useClasses, $providers] = $this->generateFieldMethods($targetClass);

        $classProvider = $providers . ($providers !== "" ? ", " : "") .
            $this->nsFixture->generateClassnameProvider($targetClass) . "::class";

        return [
            "namespace" => $namespace,
            "classname" => $classBuilder,
            "code" => $this->formatCodeForBuilderClass(
                $targetClass,
                $namespace,
                $classBuilder,
                $classProvider,
                $useClasses,
                $fieldMethods
            ),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array<mixed>
     */
    protected function generateFieldMethods(ReflectionDomainClass $targetClass): array
    {
        $fieldDataForCode = array_map(
            [$this, 'mapDataForFieldMethod'],
            $targetClass->getEntityFields()
        );

        $useClasses = [];
        $providers = [];
        $fieldMethods = "";
        foreach ($fieldDataForCode as $field) {
            $this->output->writeln(
                "<comment>    Method for parameter...</comment> " .
                $field['name'] . " (" . $field["type"] . ")"
            );

            $fieldMethods .= $this->formatCodeForFieldMethod($field);

            if ($field['provider'] !== "") {
                $providers[] = $field['provider'];
            }
            foreach ($field['classes'] as $fldClass) {
                $useClasses[$fldClass] = true;
            }
        }
        $fieldMethods = trim($fieldMethods);

        $uses = "";
        if (count($useClasses) > 0) {
            ksort($useClasses);
            $uses = "use " . implode(";\nuse ", array_keys($useClasses)) . ";";
        }
        $classProviders = "";
        if (count($providers) > 0) {
            $classProviders = implode('::class, ', $providers) . "::class";
        }

        return [$fieldMethods, $uses, $classProviders];
    }

    /**
     * {@inheritdoc}
     *
     * @param array<string> $field
     */
    protected function formatCodeForFieldMethod(array $field): string
    {
        return <<<CODE


    public function with{$field["capital"]}(\${$field["name"]}): self
    {
        return \$this->with(
            '{$field["name"]}',
            Value::set({$field["casting"]}\${$field["name"]}){$field["extra"]}
        );
    }
CODE;
    }

    protected function formatCodeForBuilderClass(
        ReflectionClass $targetClass,
        string $namespace,
        string $classBuilder,
        string $classProvider,
        string $useClasses,
        string $fieldMethods
    ): string {
        return <<<CODE
<?php declare(strict_types=1);

namespace ${namespace};

use Parchex\Lump\Fixtures\Charger\Value;
use Parchex\Lump\Fixtures\ObjectBuilder;
use {$targetClass->getName()};
{$useClasses}

/**
 * @method \\{$targetClass->getName()} build()
 */
class {$classBuilder} extends ObjectBuilder
{
    public static function create(): self
    {
        return static::builder(
            {$targetClass->getShortName()}::class,
            [{$classProvider}] // TODO: review providers
        );
    }

    {$fieldMethods}
}

CODE;
    }

    /**
     * {@inheritdoc}
     *
     * @return array<mixed>
     */
    protected function mapDataForFieldMethod(ReflectionParameter $parameter): array
    {
        $type = $this->getParameterType($parameter);

        $value = [
            "name" => $parameter->getName(),
            "capital" => ucfirst($parameter->getName()),
            "provider" => "",
            "casting" => "",
            "extra" => "",
            "type" => $type,
            "classes" => [],
        ];

        switch ($type) {
            case "array":
                $value["extra"] = $this->formatCodeForTypeArray();
                break;
            case "bool":
                $value["extra"] = $this->formatCodeForTypeBoolean();
                break;
            case "datetime":
                $value["extra"] = $this->formatCodeForTypeDateTime();
                break;
            case "identifier":
                $typedClass = $this->getFieldTypedClass($parameter);

                $value["classes"][] = $typedClass->getName();
                $value["extra"] = $this->formatCodeForTypeIdentifier($typedClass);
                break;
            case "object":
                $typedClass = $this->getFieldTypedClass($parameter);
                $namespace = $this->nsFixture->generateNamespace($typedClass);
                $classBuilder = $this->nsFixture->generateClassnameBuilder($typedClass);

                if (strtolower($parameter->getName()) === strtolower($typedClass->getShortName())) {
                    $classProvider = $this->nsFixture->generateClassnameProvider($typedClass);
                    $value["provider"] = $classProvider;
                    $value["classes"][] = $namespace . '\\' . $classProvider;
                }
                $value["classes"][] = $typedClass->getName();
                $value["classes"][] = $namespace . '\\' . $classBuilder;
                $value["extra"] = $this->formatCodeForTypeObject($typedClass, $classBuilder);
                break;
            default:
                $value["casting"] = "(" . $type . ")";
        }

        return $value;
    }

    protected function getParameterType(ReflectionParameter $parameter): string
    {
        /** @var ?ReflectionNamedType $type */
        $type = $parameter->getType();
        if ($type === null) {
            throw new LogicException(
                "Parameter '{$parameter->getName()}' without type definition"
            );
        }

        $name = "";
        if ($type->isBuiltin()) {
            $name = $type->getName();
        } else {
            if ($type->getName() === DateTime::class) {
                $name = "datetime";
            } elseif (is_a($type->getName(), Identifier::class, true)) {
                $name = "identifier";
            } else {
                $name = "object";
            }
        }

        return $name;
    }

    protected function formatCodeForTypeArray(): string
    {
        return <<<CODE

                ->collection()
                // TODO: Setup instances builder for each element in collection
                /*
                ->instance(
                    Entity::class,
                    function(\$value) {
                        return EntityBuilder::create()
                        ->withEntityId(\$value)
                        ->build();
                    }
                )
                */
CODE;
    }

    protected function formatCodeForTypeBoolean(): string
    {
        return "->boolean()";
    }

    protected function formatCodeForTypeDateTime(): string
    {
        return "->datetime()";
    }

    protected function getFieldTypedClass(ReflectionParameter $parameter): ReflectionClass
    {
        /** @var class-string<stdClass>|stdClass $type */
        $type = $parameter->getType();

        return new ReflectionClass($type ?? stdClass::class);
    }

    protected function formatCodeForTypeIdentifier(ReflectionClass $typedClass): string
    {
        return "->identifier(" . $typedClass->getShortName() . "::class)";
    }

    protected function formatCodeForTypeObject(
        ReflectionClass $typedClass,
        string $classBuilder
    ): string {
        return <<<CODE

                ->instance(
                    {$typedClass->getShortName()}::class,
                    function(\$value) {
                        // TODO: Review building of instance for {$typedClass->getShortName()}
                        return {$classBuilder}::create()
                            ->with{$typedClass->getShortName()}Id(\$value)
                            ->build();
                    }
                )
CODE;
    }
}
