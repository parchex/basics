<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Command;

use LogicException;
use ReflectionClass;
use ReflectionParameter;

class ReflectionDomainClass extends ReflectionClass
{
    /**
     * @return array<ReflectionParameter>
     */
    public function getEntityFields(): array
    {
        $constructor = $this->getConstructor();
        if (is_null($constructor)) {
            throw new LogicException(
                $this->getName() . " without constructor"
            );
        }

        return $constructor->getParameters();
    }
}
