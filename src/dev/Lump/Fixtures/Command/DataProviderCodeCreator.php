<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Command;

use LogicException;
use Parchex\Common\DateTime;
use Parchex\Common\Enum;
use Parchex\Core\Domain\Identifier;
use ReflectionClass;
use ReflectionNamedType;
use ReflectionParameter;
use Symfony\Component\Console\Output\OutputInterface;

class DataProviderCodeCreator implements ClassCodeGenerator
{
    /**
     * @var FixtureNamespace
     */
    private $nsFixture;
    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(
        FixtureNamespace $nsFixture,
        OutputInterface $output
    ) {
        $this->nsFixture = $nsFixture;
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     *
     * @return array<mixed>
     */
    public function generateClassCode(ReflectionDomainClass $targetClass): array
    {
        $namespace = $this->nsFixture->generateNamespace($targetClass);
        $classBuilder = $this->nsFixture->generateClassnameBuilder($targetClass);
        $classProvider = $this->nsFixture->generateClassnameProvider($targetClass);

        $this->output->writeln(
            "<info>  Generating class for fixture data provider...</info> " .
            $namespace . '\\' . $classProvider
        );

        [$fieldMethods, $useClasses] = $this->generateFieldMethods($targetClass);

        return [
            "namespace" => $namespace,
            "classname" => $classProvider,
            "code" => $this->formatCodeForProviderClass(
                $targetClass,
                $namespace,
                $classBuilder,
                $classProvider,
                $useClasses,
                $fieldMethods
            ),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param array<string> $field
     */
    protected function formatCodeForFieldMethod(array $field): string
    {
        return <<<CODE


    public function {$field['name']}(): {$field['casting']}
    {
        // TODO: Review generator for field '{$field['name']}'
{$field['code']}
    }
CODE;
    }

    /**
     * {@inheritdoc}
     *
     * @return array<mixed>
     */
    protected function mapDataForFieldMethod(ReflectionParameter $parameter): array
    {
        $type = $this->getParameterType($parameter);

        $value = [
            "name" => $parameter->getName(),
            "casting" => $type,
            "code" => "",
            "type" => $type,
            "classes" => [],
        ];

        switch ($type) {
            case 'float':
                $value["casting"] = $type;
                $value["code"] = <<<CODE
        return (float)\$this->generator->randomFloat(2);
CODE;
                break;
            case "string":
                $value["casting"] = $type;
                $value["code"] = <<<CODE
        return \$this->generator->words(3, true);
CODE;
                break;
            case "int":
                $value["casting"] = $type;
                $value["code"] = <<<CODE
        return (int)\$this->generator->numberBetween(0, 100);
CODE;
                break;
            case "bool":
                $value["casting"] = $type;
                $value["code"] = <<<CODE
        return \$this->generator->boolean;
CODE;
                break;
            case "enum":
                $typedClass = $this->getFieldTypedClass($parameter);
                $value["casting"] = $typedClass->getShortName();
                $value["classes"][] = $typedClass->getName();

                $value["code"] = <<<CODE
        return \$this->generator->randomElement({$typedClass->getShortName()}::values());
CODE;
                break;
            case "datetime":
                $typedClass = $this->getFieldTypedClass($parameter);
                $value["casting"] = $typedClass->getShortName();
                $value["classes"][] = $typedClass->getName();

                $value["code"] = <<<CODE
        return {$typedClass->getShortName()}::fromString(
            \$this->generator->date({$typedClass->getShortName()}::FORMAT)
        );
CODE;
                break;
            case "identifier":
                $typedClass = $this->getFieldTypedClass($parameter);

                $value["casting"] = $typedClass->getShortName();
                $value["classes"][] = $typedClass->getName();

                $value["code"] = <<<CODE
        return {$typedClass->getShortName()}::fromString(\$this->generator->uuid);
CODE;
                break;
            case "object":
                $typedClass = $this->getFieldTypedClass($parameter);
                $namespace = $this->nsFixture->generateNamespace($typedClass);
                $classProvider = $this->nsFixture->generateClassnameProvider($typedClass);

                $value["casting"] = $typedClass->getShortName();
                $value["classes"][] = $typedClass->getName();
                $value["classes"][] = $namespace . '\\' . $classProvider;

                $factoryMethod = lcfirst($typedClass->getShortName());

                $comment =
                    strtolower($parameter->getName()) === strtolower($typedClass->getShortName())
                        ? "// TODO: Remove this method and add {$classProvider}::class to builder"
                        : "";

                $value["code"] = <<<CODE
        {$comment}
        return {$classProvider}::{$factoryMethod}();
CODE;
                break;
            case 'array':
                $value["code"] = <<<CODE
        // TODO: Generate doc... * @return Entity[]
        // TODO: Change EntityBuilder::class for specific builder
        return static::entitiesBetween(EntityBuilder::class, \$min, \$max);
CODE;
                break;
        }

        return $value;
    }

    protected function getParameterType(ReflectionParameter $parameter): string
    {
        $type = $this->getReflectionNamedType($parameter);

        if ($type->isBuiltin()) {
            return $type->getName();
        }

        if ($type->getName() === DateTime::class) {
            return "datetime";
        }
        if (is_a($type->getName(), Enum::class, true)) {
            return "enum";
        }
        if (is_a($type->getName(), Identifier::class, true)) {
            return "identifier";
        }

        return "object";
    }

    protected function getFieldTypedClass(ReflectionParameter $parameter): ReflectionClass
    {
        /** @var class-string $className */
        $className = $this->getReflectionNamedType($parameter)->getName();

        return new ReflectionClass($className);
    }

    /**
     * {@inheritdoc}
     *
     * @return array<mixed>
     */
    private function generateFieldMethods(ReflectionDomainClass $targetClass): array
    {
        $fieldDataForCode = array_map(
            [$this, 'mapDataForFieldMethod'],
            $targetClass->getEntityFields()
        );

        $useClasses = [];
        $fieldMethods = "";
        foreach ($fieldDataForCode as $field) {
            $this->output->writeln(
                "<comment>    Method for parameter...</comment> " .
                $field['name'] . " (" . $field["type"] . ")"
            );

            $fieldMethods .= $this->formatCodeForFieldMethod($field);
            // Generate unique list with used classes
            $useClasses = array_replace($useClasses, array_flip($field['classes'])) ?? [];
        }
        $fieldMethods = trim($fieldMethods);

        $uses = "";
        if ($useClasses !== []) {
            ksort($useClasses);
            $uses = "use " . implode(";\nuse ", array_keys($useClasses)) . ";";
        }

        return [$fieldMethods, $uses];
    }

    private function formatCodeForProviderClass(
        ReflectionDomainClass $targetClass,
        string $namespace,
        string $classBuilder,
        string $classProvider,
        string $useClasses,
        string $fieldMethods
    ): string {
        $factory = lcfirst($targetClass->getShortName());

        return <<<CODE
<?php declare(strict_types=1);

namespace ${namespace};

use Parchex\Lump\Fixtures\Faker;
use {$targetClass->getName()};
{$useClasses}

class ${classProvider} extends Faker
{
    public static function {$factory}(): {$targetClass->getShortName()}
    {
        return static::entity({$classBuilder}::class);
    }

    {$fieldMethods}
}

CODE;
    }

    private function getReflectionNamedType(ReflectionParameter $parameter): ReflectionNamedType
    {
        /** @var ?ReflectionNamedType $type */
        $type = $parameter->getType();
        if ($type === null) {
            throw new LogicException("Parameter '{$parameter->getName()}' without type definition");
        }

        return $type;
    }
}
