<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Command;

interface ClassCodeGenerator
{
    /**
     * @return array<mixed>
     */
    public function generateClassCode(ReflectionDomainClass $targetClass): array;
}
