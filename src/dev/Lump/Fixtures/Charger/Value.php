<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Charger;

use Parchex\Common\DateTime;

class Value
{
    /**
     * Literal for value reference meaning that not have association
     */
    public const NULL = 'null';
    /**
     * Tokens can define a collection values for a string
     */
    private const COLLECTION_TOKENS = ';,| ';
    /**
     * @var mixed
     */
    private $value;

    /**
     * @param mixed $value
     */
    final private function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param mixed $value
     *
     * @return static
     */
    public static function set($value): self
    {
        return new static($value);
    }

    /**
     * @param mixed $value
     *
     * {@inheritdoc}
     */
    public static function equalsNull($value): bool
    {
        return $value === null || $value === Value::NULL;
    }

    /**
     * @param mixed $value
     *
     * {@inheritdoc}
     */
    public static function isNullableOf($value, string $class): bool
    {
        return static::isA($value, $class) || static::equalsNull($value);
    }

    /**
     * @param mixed $value
     *
     * {@inheritdoc}
     */
    public static function isA($value, string $class): bool
    {
        return (is_object($value) && is_a($value, $class))
            || (is_array($value) && self::isA(reset($value), $class));
    }

    /**
     * @param mixed $value
     *
     * {@inheritdoc}
     */
    public static function isNotA($value, string $class): bool
    {
        return !static::isA($value, $class) && !static::equalsNull($value);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    public function boolean(): self
    {
        $this->value = filter_var(
            $this->value,
            FILTER_VALIDATE_BOOLEAN,
            FILTER_NULL_ON_FAILURE
        );

        return $this;
    }

    public function nullable(): self
    {
        if ($this->isNull()) {
            $this->value = null;
        }

        return $this;
    }

    public function identifier(string $class): self
    {
        $this->instance(
            $class,
            static function ($value, $class) {
                return $class::fromString((string) $value);
            }
        );

        return $this;
    }

    public function instance(string $class, callable $callableFactory): self
    {
        if ($this->isNull()) {
            $this->value = null;

            return $this;
        }

        if (is_array($this->value)) {
            $this->value = array_map(
                static function ($value) use ($class, $callableFactory) {
                    return static::set($value)
                        ->newInstance($class, $callableFactory)
                        ->get();
                },
                $this->value
            );

            return $this;
        }

        return $this->newInstance($class, $callableFactory);
    }

    public function datetime(string $class = DateTime::class): self
    {
        $this->instance(
            $class,
            static function ($value, $class) {
                return $class::fromString((string) $value);
            }
        );

        return $this;
    }

    public function collection(): self
    {
        if (is_iterable($this->value)) {
            return $this;
        }
        if (is_object($this->value)) {
            $this->value = [$this->value];

            return $this;
        }

        $tok = strtok((string) $this->value, self::COLLECTION_TOKENS);
        $this->value = [];
        while ($tok !== false) {
            $this->value[] = $tok;
            $tok = strtok(self::COLLECTION_TOKENS);
        }

        return $this;
    }

    public function isNull(): bool
    {
        return static::equalsNull($this->value);
    }

    public function isInstanceOf(string $class): bool
    {
        return static::isA($this->value, $class);
    }

    public function isNotInstanceOf(string $class): bool
    {
        return static::isNotA($this->value, $class);
    }

    public function isInstanceNullableOf(string $class): bool
    {
        return static::isNullableOf($this->value, $class);
    }

    private function newInstance(string $class, callable $factory): self
    {
        if ($this->isNotInstanceOf($class)) {
            $this->value = $factory($this->value, $class);
        }

        return $this;
    }
}
