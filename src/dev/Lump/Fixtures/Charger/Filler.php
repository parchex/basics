<?php declare(strict_types=1);

namespace Parchex\Lump\Fixtures\Charger;

use Faker\Generator;

class Filler
{
    /**
     * @var Generator
     */
    protected $generator;
    /**
     * List of values for each param in constructor
     *
     * @var array<?Value> $fields
     */
    protected $fields;

    /**
     * {@inheritdoc}
     *
     * @param array<string> $fields
     */
    public function __construct(array $fields, Generator $generator)
    {
        $this->generator = $generator;
        $this->fields = array_fill_keys($fields, null);

        $this->refill();
    }

    /**
     * Generate fake data for properties of class, with data
     * providers generators that match with the name or property
     *
     * {@inheritdoc}
     */
    public function refill(): self
    {
        foreach (array_keys($this->fields) as $name) {
            $this->fields[$name] = Value::set($this->generator->format($name));
        }

        return $this;
    }

    public function valueOf(string $name): Value
    {
        return $this->fields[$name] ?? Value::set(null);
    }

    /**
     * @param mixed $value
     *
     * {@inheritdoc}
     */
    public function withValue(string $prop, $value): self
    {
        return $this->with($prop, Value::set($value));
    }

    /**
     * Fill property with specific value
     *
     * {@inheritdoc}
     */
    public function with(string $prop, Value $value): self
    {
        if (array_key_exists($prop, $this->fields)) {
            $this->fields[$prop] = $value;
        }

        return $this;
    }

    /**
     * @return array<mixed>
     */
    public function toArray(): array
    {
        return array_map(
            static function (Value $value) {
                return $value->get();
            },
            $this->fields
        );
    }
}
