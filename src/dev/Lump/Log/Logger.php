<?php declare(strict_types=1);

namespace Parchex\Lump\Log;

use Psr\Log\LoggerInterface;

class Logger
{
    /**
     * @var LoggerInterface
     */
    private static $self;

    /**
     * {@inheritdoc}
     *
     * @param array<mixed> $settings
     */
    public static function instance(array $settings = []): LoggerInterface
    {
        static::$self = static::$self ?? Monolog\Logger::build($settings);

        return static::$self;
    }
}
