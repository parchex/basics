<?php declare(strict_types=1);

namespace Parchex\Lump\Log\Monolog;

use Bramus\Monolog\Formatter\ColoredLineFormatter;
use InvalidArgumentException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as Monolog;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\UidProcessor;
use Psr\Log\LoggerInterface;

class Logger
{
    /**
     * $settings: Options for configuration
     * * name: Channel name for this logs
     * * path: where output logs
     * * level: what level logs to show
     * * dev_mode: show extra info in logs (memory, colored format)
     *
     * {@inheritdoc}
     *
     * @param array<mixed> $settings
     */
    public static function build(array $settings): LoggerInterface
    {
        if ($settings === []) {
            throw new InvalidArgumentException('Empty logger settings');
        }

        $logger = new Monolog($settings['name']);

        $logger->pushProcessor(new UidProcessor());
        if ((bool) $settings['dev_mode']) {
            $logger->pushProcessor(new MemoryUsageProcessor());
            $logger->pushProcessor(new MemoryPeakUsageProcessor());
        }

        $handler = new StreamHandler($settings['path'], $settings['level']);
        if ((bool) $settings['dev_mode']) {
            $handler->setFormatter(new ColoredLineFormatter());
        }
        $logger->pushHandler($handler);

        return $logger;
    }
}
