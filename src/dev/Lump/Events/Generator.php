<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

trait Generator
{
    use League\Generator;
}
