<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

use Parchex\Common\DateTime;

/**
 * Interface to define event types
 */
interface Event
{
    public function name(): string;

    /**
     * @return DateTime Moment when event happened
     */
    public function occurredOn(): DateTime;
}
