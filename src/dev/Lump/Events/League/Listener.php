<?php declare(strict_types=1);

namespace Parchex\Lump\Events\League;

use League\Event\AbstractListener;

abstract class Listener extends AbstractListener
{
}
