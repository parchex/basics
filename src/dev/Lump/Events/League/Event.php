<?php declare(strict_types=1);

namespace Parchex\Lump\Events\League;

use League\Event\AbstractEvent as LeagueEvent;

abstract class Event extends LeagueEvent
{
}
