<?php declare(strict_types=1);

namespace Parchex\Lump\Events\League;

use League\Event\GeneratorTrait;

trait Generator
{
    use GeneratorTrait {
        addEvent as record;
    }
}
