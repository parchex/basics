<?php declare(strict_types=1);

namespace Parchex\Lump\Events\League;

use League\Event\Emitter;
use League\Event\EmitterInterface;
use Parchex\Lump\Events\Event as EventInterface;
use Parchex\Lump\Events\Publisher;
use Parchex\Lump\Events\Subscriber;

class Manager implements Publisher, Subscriber
{
    /**
     * @var EmitterInterface
     */
    private $eventManager;

    public function __construct(EmitterInterface $eventManager = null)
    {
        $this->eventManager = $eventManager ?? new Emitter();
    }

    /**
     * {@inheritdoc}
     */
    public function trigger(EventInterface $event, ...$params): Publisher
    {
        $this->eventManager->emit($event, ...$params);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function attach(
        string $eventName,
        $callback,
        int $priority = self::PRIORITY_NORMAL
    ): Subscriber {
        $this->eventManager->addListener($eventName, $callback, $priority);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function detach($eventName, $callback): Subscriber
    {
        $this->eventManager->removeListener($eventName, $callback);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $eventName
     */
    public function loose(string $eventName): Subscriber
    {
        $this->eventManager->removeAllListeners($eventName);

        return $this;
    }
}
