<?php declare(strict_types=1);

namespace Parchex\Lump\Events\League;

use League\Tactician\Middleware;
use Parchex\Lump\Events\Persistence\PersistAllEventsEmittedMiddleware as EventsMiddleware;

class PersistAllEventsEmittedTacticianMiddleware extends EventsMiddleware implements Middleware
{
    /**
     * {@inheritdoc}
     *
     * @param object $command
     *
     * @param callable $next
     */
    public function execute($command, callable $next)
    {
        return $this->handle($command, $next);
    }
}
