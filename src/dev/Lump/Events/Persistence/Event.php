<?php declare(strict_types=1);

namespace Parchex\Lump\Events\Persistence;

use Parchex\Common\DateTime;
use Parchex\Core\Domain\DomainEvent;

class Event
{
    /**
     * @var string
     */
    private $identifier;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $body;
    /**
     * @var DateTime
     */
    private $occurredOn;

    public function __construct(
        string $typeName,
        string $body,
        DateTime $occurredOn
    ) {
        $this->type = $typeName;
        $this->body = $body;
        $this->occurredOn = $occurredOn;
    }

    public static function from(DomainEvent $domainEvent): self
    {
        return new self(
            $domainEvent->name(),
            (string) json_encode($domainEvent, JSON_PRETTY_PRINT),
            $domainEvent->occurredOn()
        );
    }

    public function identifier(): string
    {
        return $this->identifier;
    }

    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return array<string>
     */
    public function body(): array
    {
        return json_decode($this->body) ?? [];
    }

    public function occurredOn(): DateTime
    {
        return $this->occurredOn;
    }
}
