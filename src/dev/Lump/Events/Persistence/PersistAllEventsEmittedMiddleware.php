<?php declare(strict_types=1);

namespace Parchex\Lump\Events\Persistence;

use Parchex\Core\Domain\DomainEvent;
use Parchex\Lump\Events\Subscriber;

class PersistAllEventsEmittedMiddleware
{
    /**
     * @var EventStorage
     */
    private $eventStorage;
    /**
     * @var Subscriber
     */
    private $eventSubscriber;
    /**
     * @var AllEventsListener
     */
    private $listener;

    public function __construct(
        EventStorage $eventStorage,
        Subscriber $eventSubscriber,
        AllEventsListener $listener
    ) {
        $this->eventStorage = $eventStorage;
        $this->eventSubscriber = $eventSubscriber;
        $this->listener = $listener;
    }

    /**
     * @param mixed $message
     *
     * @return mixed
     *
     * {@inheritdoc}
     */
    public function handle($message, callable $next)
    {
        $this->eventSubscriber->attach('*', $this->listener);

        $returnValue = $next($message);

        $this->eventSubscriber->detach('*', $this->listener);

        /** @var DomainEvent $event */
        foreach ($this->listener->release() as $event) {
            $this->eventStorage->append(Event::from($event));
        }

        return $returnValue;
    }
}
