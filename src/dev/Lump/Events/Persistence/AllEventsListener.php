<?php declare(strict_types=1);

namespace Parchex\Lump\Events\Persistence;

use Parchex\Lump\Events\AbstractListener;
use Parchex\Lump\Events\Event;

class AllEventsListener extends AbstractListener
{
    /**
     * @var array<Event>
     */
    private $events;

    public function __construct()
    {
        $this->events = [];
    }

    /**
     * {@inheritdoc}
     */
    public function handle($event): void
    {
        $this->events[] = $event;
    }

    /**
     * @return array<Event>
     */
    public function release(): array
    {
        [$events, $this->events] = [$this->events(), []];

        return $events;
    }

    /**
     * @return array<Event>
     */
    public function events(): array
    {
        return $this->events;
    }
}
