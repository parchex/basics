<?php declare(strict_types=1);

namespace Parchex\Lump\Events\Persistence;

interface EventStorage
{
    public function append(Event $event): void;
}
