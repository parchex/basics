<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

abstract class AbstractListener extends League\Listener
{
    /**
     * @param Event $event
     */
    abstract public function handle($event): void;
}
