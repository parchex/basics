<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

use Parchex\Common\ContentImmutable;
use Parchex\Common\ContentImmutableTrait;
use Parchex\Common\DateTime;

abstract class AbstractEvent extends League\Event implements Event, ContentImmutable
{
    use ContentImmutableTrait;

    /**
     * {@inheritdoc}
     *
     * @param array<string, mixed> $eventData
     */
    public function __construct(array $eventData = [], DateTime $occurredOn = null)
    {
        $this->prop = ['occurred_on' => $occurredOn ?? DateTime::now()] + $eventData;
    }

    public function name(): string
    {
        return static::class;
    }

    public function occurredOn(): DateTime
    {
        return $this['occurred_on'];
    }
}
