<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

interface Subscriber
{
    public const PRIORITY_NORMAL = 0;

    /**
     * Attaches a listener to an event
     *
     * @param string $eventName the event to attach too
     * @param AbstractListener|callable $callback a callable function
     * @param int $priority the priority at which the $callback executed
     *
     * @return static
     */
    public function attach(
        string $eventName,
        $callback,
        int $priority = self::PRIORITY_NORMAL
    ): Subscriber;

    /**
     * Detaches a listener from an event
     *
     * @param string $eventName the event to attach too
     * @param AbstractListener|callable $callback a callable function
     *
     * @return static
     */
    public function detach(string $eventName, $callback);

    /**
     * Clear all listeners for a given event
     *
     * @return static
     */
    public function loose(string $eventName);
}
