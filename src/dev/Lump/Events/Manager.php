<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

class Manager extends League\Manager
{
    /**
     * @var self
     */
    private static $self;

    private function __construct()
    {
        parent::__construct();
    }

    public static function publisher(): Publisher
    {
        return static::instance();
    }

    public static function subscriber(): Subscriber
    {
        return static::instance();
    }

    protected static function instance(): self
    {
        self::$self = self::$self ?? new self();

        return self::$self;
    }
}
