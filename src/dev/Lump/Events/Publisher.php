<?php declare(strict_types=1);

namespace Parchex\Lump\Events;

interface Publisher
{
    /**
     * Trigger an event
     * {@inheritdoc}
     *
     * @param mixed ...$params
     *
     * @return static
     */
    public function trigger(Event $event, ...$params);
}
